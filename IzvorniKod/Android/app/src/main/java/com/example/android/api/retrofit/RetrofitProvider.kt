package com.example.android.api.retrofit

import com.example.android.api.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitProvider private constructor(){
    // https://upravljanje-kucnim-otpadom.herokuapp.com/ http://10.0.2.2:8080/
    private val baseUrl = "https://upravljanje-kucnim-otpadom.herokuapp.com/" //setup for emulator testing
    val userApi: UserApi
    val inquiriesApi: InquiriesApi
    val productsApi: ProductsApi
    val disposalPlaceApi: DisposalPlaceApi
    val removalsApi: RemovalsApi

    companion object {
        @Volatile private var instance: RetrofitProvider? = null

        fun getInstance() =
                instance ?: synchronized(this) {
                    instance ?: RetrofitProvider().also { instance = it }
                }
    }

    init{
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                //.addInterceptor(BasicAuthInterceptor(username, password))
                .build()
        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        userApi = retrofit.create(UserApi::class.java)
        inquiriesApi = retrofit.create(InquiriesApi::class.java)
        productsApi = retrofit.create(ProductsApi::class.java)
        disposalPlaceApi = retrofit.create(DisposalPlaceApi::class.java)
        removalsApi = retrofit.create(RemovalsApi::class.java)
    }


}