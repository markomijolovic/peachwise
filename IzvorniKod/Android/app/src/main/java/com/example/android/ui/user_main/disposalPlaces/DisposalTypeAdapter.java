package com.example.android.ui.user_main.disposalPlaces;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.R;
import com.example.android.models.DisposalType;

import java.util.List;

public class DisposalTypeAdapter extends BaseAdapter {

    private final Context mContext;
    private final List<DisposalType> disposalTypes;

    public DisposalTypeAdapter(Context context, List<DisposalType> disposalTypes) {
        this.mContext = context;
        this.disposalTypes = disposalTypes;
    }

    @Override
    public int getCount() {
        return disposalTypes.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final DisposalType disposalType = disposalTypes.get(position);
        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.disposal_type, null);
        }
        final ImageView icon = convertView.findViewById(R.id.iv_iconType);
        final TextView label = convertView.findViewById(R.id.tv_labelType);
        icon.setImageResource(disposalType.getResourceId());
        label.setText(disposalType.getLabel());
        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }



}

