package com.example.android.utils

import com.example.android.R
import com.example.android.enums.WasteCategory
import com.example.android.models.DisposalType

object DisposalTypeProvider {
    val disposalTypeMap = mapOf(
            WasteCategory.MIXED_COMUNAL to DisposalType("Mijesani otpad", R.drawable.mixed),
            WasteCategory.BIOWASTE to DisposalType("Bio otpad", R.drawable.bio_waste),
            WasteCategory.PAPER_CARDBOARD to DisposalType("Papir i karton", R.drawable.paper),
            WasteCategory.PLASTIC_METAL to DisposalType("Plastika i metal", R.drawable.plastic_metal),
            WasteCategory.BULKY to DisposalType("Glomazni otpad", R.drawable.bluky),
            WasteCategory.TEXTILE to DisposalType("Tekstil", R.drawable.textile),
            WasteCategory.DANGEROUS to DisposalType("Opasni proizvodi", R.drawable.biohazard)
    )
}