package com.example.android.ui.user_main.inquiries.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.R;
import com.example.android.models.Inquiry;
import com.example.android.ui.user_main.RespondPressedListener;

import java.util.ArrayList;
import java.util.List;

public class EmployeeComplaintAdapter extends RecyclerView.Adapter<EmployeeComplaintAdapter.InquiryViewHolder> {

    private List<Inquiry> complaintList = new ArrayList<>();
    private Context context;
    private RespondPressedListener respondPressedListener;

    public EmployeeComplaintAdapter(Context context, RespondPressedListener respondPressedListener) {
        this.context = context;
        this.respondPressedListener = respondPressedListener;
    }

    public void updateInquiryList(List<Inquiry> inquiryList) {
        this.complaintList = inquiryList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public InquiryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.complaint_employee_item, parent, false);
        return new InquiryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InquiryViewHolder holder, int position) {
        Inquiry complaint = complaintList.get(position);
        holder.complaintText.setText(complaint.getRequestText());
        holder.complaintDate.setText(formatDate(complaint.getTimestamp()));
        holder.button.setOnClickListener(v ->
                respondPressedListener.onRespondPressed(R.id.action_nav_requests_to_answerComplaintFragment, complaint));
    }

    @Override
    public int getItemCount() {
        return complaintList.size();
    }

    class InquiryViewHolder extends RecyclerView.ViewHolder {

        private final TextView complaintText;
        private final TextView complaintDate;
        private final Button button;

        InquiryViewHolder(@NonNull View itemView) {
            super(itemView);
            complaintText = itemView.findViewById(R.id.tv_complaint_text);
            complaintDate = itemView.findViewById(R.id.tv_complaint_date);
            button = itemView.findViewById(R.id.btn_answer);
        }
    }

    private String formatDate(String date) {
        String month = date.substring(5,7);
        String day = date.substring(8,10);
        String year = date.substring(0,4);
        return day + "." + month + "." + year;
    }
}
