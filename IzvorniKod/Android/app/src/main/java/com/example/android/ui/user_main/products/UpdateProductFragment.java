package com.example.android.ui.user_main.products;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.android.R;
import com.example.android.enums.WasteCategory;
import com.example.android.models.Product;
import com.example.android.models.Result;
import com.example.android.repository.AppRepository;
import com.example.android.ui.BaseFragment;

//TODO: Jaksa -> napraviti layout i logiku, zaposlenik ovdje moze samo mijenjati WasteCategory
public class UpdateProductFragment extends BaseFragment {
    WasteCategory selectedWasteCategory;
    AppRepository repository;

    private Long productId;
    private String productNameString;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        repository = AppRepository.Companion.getInstance();
        return inflater.inflate(R.layout.fragment_update_product, container, false);

    }

    //ovdje je najbolje inicijalizirat viowe, ne treba ti onaj root. sranje, nego getActivity()
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView productName = getActivity().findViewById(R.id.update_product_name_old);
        Button changeCategoryButton = getActivity().findViewById(R.id.apply_category_button);
        ImageView oldWasteCategoryImg = getActivity().findViewById(R.id.update_product_icon_old);
        if (getArguments().get("wasteCategory") == WasteCategory.BULKY.toString()) {
            oldWasteCategoryImg.setImageResource(R.drawable.bluky);
        } else if (getArguments().get("wasteCategory") == WasteCategory.BIOWASTE.toString()) {
            oldWasteCategoryImg.setImageResource(R.drawable.bio_waste);
        } else if (getArguments().get("wasteCategory") == WasteCategory.DANGEROUS.toString()) {
            oldWasteCategoryImg.setImageResource(R.drawable.biohazard);
        } else if (getArguments().get("wasteCategory") == WasteCategory.MIXED_COMUNAL.toString()) {
            oldWasteCategoryImg.setImageResource(R.drawable.mixed);
        } else if (getArguments().get("wasteCategory") == WasteCategory.PAPER_CARDBOARD.toString()) {
            oldWasteCategoryImg.setImageResource(R.drawable.paper);
        } else if (getArguments().get("wasteCategory") == WasteCategory.PLASTIC_METAL.toString()) {
            oldWasteCategoryImg.setImageResource(R.drawable.plastic_metal);
        } else if (getArguments().get("wasteCategory") == WasteCategory.TEXTILE.toString()) {
            oldWasteCategoryImg.setImageResource(R.drawable.textile);
        }

        productNameString = (String) getArguments().get("productName");
        productId = (Long) getArguments().get("productId");

        productName.setText(productNameString);
        Spinner spinner = (Spinner) getActivity().findViewById(R.id.waste_category_change);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.waste_type_list, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            //kaj nije da ima novih vrsta otpada vec?
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: {
                        selectedWasteCategory = WasteCategory.BULKY;
                        break;
                    }
                    case 1: {
                        selectedWasteCategory = WasteCategory.PLASTIC_METAL;
                        break;
                    }
                    case 2: {
                        selectedWasteCategory = WasteCategory.PAPER_CARDBOARD;
                        break;
                    }
                    case 3: {
                        selectedWasteCategory = WasteCategory.BIOWASTE;
                        break;
                    }
                    case 4: {
                        selectedWasteCategory = WasteCategory.MIXED_COMUNAL;
                        break;
                    }
                    case 5: {
                        selectedWasteCategory = WasteCategory.TEXTILE;
                        break;
                    }
                    case 6: {
                        selectedWasteCategory = WasteCategory.DANGEROUS;
                        break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        changeCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product product = new Product(productId, productNameString, selectedWasteCategory);
                repository.updateProduct(product);
            }
        });
    }

    @Override
    public void apiResultChanged(Result result) {
        switch (result.getResultType()) {
            case UPDATE_PRODUCT: {
                Toast.makeText(getActivity(), "Kategorija proizvoda uspješno promijenjena", Toast.LENGTH_LONG).show(); //show si zaboravio!!
            }
        }
    }
}
