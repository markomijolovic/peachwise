package com.example.android.ui.user_main;

import com.example.android.interfaces.Resultable;

public interface RespondPressedListener {
    void onRespondPressed(int action, Resultable resultable);
}
