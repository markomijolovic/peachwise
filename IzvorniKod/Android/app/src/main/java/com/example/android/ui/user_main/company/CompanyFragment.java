package com.example.android.ui.user_main.company;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.android.R;
import com.example.android.models.Company;
import com.example.android.models.Result;
import com.example.android.repository.AppRepository;
import com.example.android.ui.BaseFragment;

public class CompanyFragment extends BaseFragment {

    private AppRepository repository;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_company, container, false);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        repository = AppRepository.Companion.getInstance();
        repository.getCompanyInfo();
    }

    @Override
    public void apiResultChanged(Result result) {
        switch (result.getResultType()) {
            case GET_COMPANY_INFO: {
                TextView companyName = getActivity().findViewById(R.id.company_name);
                TextView companyAddress = getActivity().findViewById(R.id.company_address);
                TextView companyTelephone = getActivity().findViewById(R.id.company_telephone);
                TextView companyEmail = getActivity().findViewById(R.id.company_email);
                Company company = (Company) result.getSingleResult();
                companyName.setText(company.getName());
                companyAddress.setText(company.getAddress());
                companyTelephone.setText(company.getPhoneNumber());
                companyEmail.setText(company.getEmail());
            }
        }
    }
}
