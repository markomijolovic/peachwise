package com.example.android.ui.user_main.schedule;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.R;
import com.example.android.enums.WasteCategory;
import com.example.android.models.DisposalType;
import com.example.android.models.Removal;
import com.example.android.models.Role;
import com.example.android.ui.user_main.EditPressedListener;
import com.example.android.utils.DisposalTypeProvider;
import com.example.android.utils.UserStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ScheduleViewHolder> {

    private List<Removal> removals = new ArrayList<>();
    private Context context;
    private Map<WasteCategory, DisposalType> disposalMap = DisposalTypeProvider.INSTANCE.getDisposalTypeMap();
    private EditPressedListener editPressedListener;

    ScheduleAdapter(Context ct, EditPressedListener editPressedListener) {
        this.editPressedListener = editPressedListener;
        context = ct;
    }

    void updateRemovalList(List<Removal> removals) {
        this.removals = removals;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ScheduleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.schedule_row, parent, false);
        return new ScheduleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ScheduleViewHolder holder, int position) {
        DisposalType disposalType = disposalMap.get(removals.get(position).getWasteCategory());

        enableEditingIfEmployee(holder, position);

        holder.schedule.setText(removals.get(position).getTime());
        holder.blockName.setText(removals.get(position).getCityBlock());
        holder.wasteCategory.setText(disposalType.getLabel());
        holder.wasteIcon.setImageResource(disposalType.getResourceId());
    }

    @Override
    public int getItemCount() {
        return removals.size();
    }

    class ScheduleViewHolder extends RecyclerView.ViewHolder {

        TextView blockName;
        TextView wasteCategory;
        TextView schedule;
        ImageView wasteIcon;
        View edit;

        ScheduleViewHolder(@NonNull View itemView) {
            super(itemView);
            schedule = itemView.findViewById(R.id.tv_schedule);
            blockName = itemView.findViewById(R.id.tv_blockName);
            wasteCategory = itemView.findViewById(R.id.waste_category);
            wasteIcon = itemView.findViewById(R.id.iv_wasteIcon);
            edit = itemView.findViewById(R.id.btn_edit);
        }
    }

    private void enableEditingIfEmployee(ScheduleViewHolder holder, int position) {
        boolean isEmployee = UserStorage.INSTANCE.getCurrentUser().getRole() == Role.ROLE_EMPLOYEE;
        if(isEmployee) {
            holder.edit.setVisibility(View.VISIBLE);
            holder.edit.setOnClickListener(v -> editPressedListener.onEditPressed(removals.get(position)));
        }
    }
}
