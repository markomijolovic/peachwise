package com.example.android.ui.user_main.inquiries.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.R;
import com.example.android.enums.ResourceType;
import com.example.android.models.DisposalType;
import com.example.android.models.Inquiry;
import com.example.android.models.ResourceRequest;
import com.example.android.utils.DisposalTypeProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserResourceRequestAdapter extends RecyclerView.Adapter<UserResourceRequestAdapter.InquiryViewHolder> {


    private static final Map<ResourceType, String> resourceTypeMap  = new HashMap<ResourceType, String>() {{
        put(ResourceType.BAG, "Vrecica");
        put(ResourceType.BUCKET, "Kanta za smece");
        put(ResourceType.CONTAINER, "Kontejner");
    }};


    private List<Inquiry> removalREquestList = new ArrayList<>();
    private Context context;

    public UserResourceRequestAdapter(Context context) {
        this.context = context;
    }

    public void updateInquiryList(List<Inquiry> inquiryList) {
        this.removalREquestList = inquiryList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public InquiryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.resource_request_user_item, parent, false);
        return new InquiryViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull InquiryViewHolder holder, int position) {
        Inquiry inquiry = removalREquestList.get(position);
        ResourceRequest resourceRequest = inquiry.getResources().get(0);
        DisposalType disposalType = DisposalTypeProvider.INSTANCE.getDisposalTypeMap().get(resourceRequest.getWasteCategory());

        holder.quantity.setText("Kolicina: " + resourceRequest.getQuantity());
        holder.wasteCategory.setText(disposalType.getLabel());
        holder.requestText.setText(inquiry.getRequestText());
        holder.resourceType.setText(resourceTypeMap.get(resourceRequest.getResourceType()));

        holder.complaintResponse.setText(resourceRequest.getResponseText() != null ? resourceRequest.getResponseText() : "Nema odgovora");
        holder.isProcessed.setChecked(inquiry.getProcessed() != null ? inquiry.getProcessed() : false);
        holder.isApproved.setChecked(inquiry.getApproved() != null ? inquiry.getApproved() : false);
        holder.complaintDate.setText(formatDate(inquiry.getTimestamp()));
    }

    @Override
    public int getItemCount() {
        return removalREquestList.size();
    }

    class InquiryViewHolder extends RecyclerView.ViewHolder {

        private final TextView wasteCategory;
        private final TextView quantity;
        private final TextView resourceType;
        private final TextView requestText;
        private final CheckBox isApproved;
        private final CheckBox isProcessed;
        private final TextView complaintResponse;
        private final TextView complaintDate;

        InquiryViewHolder(@NonNull View itemView) {
            super(itemView);
            requestText = itemView.findViewById(R.id.tv_request_text);
            resourceType = itemView.findViewById(R.id.tv_bag_type);
            quantity = itemView.findViewById(R.id.tv_count);
            wasteCategory = itemView.findViewById(R.id.tv_waste_category);
            complaintResponse = itemView.findViewById(R.id.tv_complaint_response);
            isApproved = itemView.findViewById(R.id.cb_isApproved);
            isProcessed = itemView.findViewById(R.id.cb_isProcessed);
            complaintDate = itemView.findViewById(R.id.tv_complaint_date);
        }
    }

    private String formatDate(String date) {
        String month = date.substring(5,7);
        String day = date.substring(8,10);
        String year = date.substring(0,4);
        return day + "." + month + "." + year;
    }
}

