package com.example.android.models

data class BadRequest(
        val message: String,
        val error: String,
        val status: Int
)