package com.example.android.models;

import com.example.android.enums.ResultType;
import com.example.android.interfaces.Resultable;

import java.util.List;

public class Result {

    private ResultType resultType;
    private Resultable singleResult;
    private List<? extends Resultable> listResult;

    public Result(ResultType resultType, Resultable singleResult) {
        this.resultType = resultType;
        this.singleResult = singleResult;
    }

    public Result(ResultType resultType, List<Resultable> listResult) {
        this.resultType = resultType;
        this.listResult = listResult;
    }

    public ResultType getResultType() {
        return resultType;
    }

    public Resultable getSingleResult() {
        return singleResult;
    }

    public List<? extends Resultable> getListResult() {
        return listResult;
    }

}
