package com.example.android.repository

import android.annotation.SuppressLint
import com.example.android.api.retrofit.RetrofitProvider
import com.example.android.enums.ResultType
import com.example.android.interfaces.ApiErrorListener
import com.example.android.interfaces.ApiResultListener
import com.example.android.interfaces.Resultable
import com.example.android.interfaces.UserApiProvider
import com.example.android.models.*
import com.example.android.utils.ErrorParser
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.Credentials
import java.nio.charset.Charset


class AppRepository private constructor() : UserApiProvider {

    private val userApi = RetrofitProvider.getInstance().userApi
    private val inquiriesApi = RetrofitProvider.getInstance().inquiriesApi
    private val productsApi = RetrofitProvider.getInstance().productsApi
    private val disposalPlaceApi = RetrofitProvider.getInstance().disposalPlaceApi
    private val removalsApi = RetrofitProvider.getInstance().removalsApi

    private var authorization: String = ""
    private val resultSubscribers = arrayListOf<ApiResultListener>()
    private val errorSubscribers = arrayListOf<ApiErrorListener>()

    companion object {
        @Volatile
        private var instance: AppRepository? = null

        fun getInstance(): AppRepository {
            return instance ?: synchronized(this) {
                instance ?: AppRepository().also { instance = it }
            }
        }
    }

    fun setUsernameAndPassword(username: String, password: String) {
        authorization = Credentials.basic(username, password, Charset.defaultCharset());
    }

    fun subscribeToResult(listener: ApiResultListener) {
        resultSubscribers.add(listener)
    }

    fun unsubscribeFromResult(listener: ApiResultListener) {
        resultSubscribers.remove(listener)
    }

    fun subscribeToError(listener: ApiErrorListener) {
        errorSubscribers.add(listener)
    }

    fun unsubscribeFromError(listener: ApiErrorListener) {
        errorSubscribers.remove(listener)
    }

    /**
     * UserApiProvider
     */
    @SuppressLint("CheckResult")
    override fun getAllUsers() {
        notifyApiCallRequested()
        userApi
                .getAllUsers(authorization)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val result = Result(ResultType.ALL_USERS, it)
                    resultSubscribers.forEach { subscriber ->
                        subscriber.apiResultChanged(result)
                     }
                }
    }

    private fun notifyApiCallRequested() {
        resultSubscribers.forEach{
            it.apiResultRequested()
        }
    }

    override fun getUserWithUsername(username: String) {
        handleSingleResponseResult(
                userApi.getUserWithUsername(username, authorization), ResultType.GET_USER)
    }

    override fun updateUserWithUsername(username: String, newUser: User) {
        handleSingleResponseResult(
                userApi.updateUserWithUsername(username, newUser, authorization), ResultType.UPDATE_USER)
    }

    override fun deleteUserWithUsername(username: String) {
        handleSingleResponseResult(
                userApi.deleteUserWithUsername(username, authorization), ResultType.DELETE_USER)
    }

    override fun login() {
        handleSingleResponseResult(userApi.login(authorization), ResultType.LOGIN_USER)
    }

    override fun registerUser(user: User) {
        handleSingleResponseResult(
                userApi.registerUser(user, authorization), ResultType.REGISTER_USER)
    }

    /**
     * InquiryApiProvider
     */
    fun getAllInquiries() {
        notifyApiCallRequested()
        val disp = inquiriesApi.getAllInquiries(authorization)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val result = Result(ResultType.ALL_INQUIRIES, it)
                    resultSubscribers.forEach { subscriber ->
                        subscriber.apiResultChanged(result)
                    }
                }
    }

    fun postComplaint(complaint: Complaint) {
        val inquiry = Inquiry(InquiryBuilder(InquiryType.COMPLAINT).apply {
            requestText = complaint.requestText
        })
        postInquiry(inquiry)
    }

    fun postRemovalRequest(item: RemovalRequest) {
        val inquiry = Inquiry(InquiryBuilder(InquiryType.REMOVAL_REQUEST).apply {
            requestItems = listOf(item)
        })
        postInquiry(inquiry)
    }

    fun postResourceRequest(request: ResourceRequest) {
        val inquiry = Inquiry(InquiryBuilder(InquiryType.RESOURCE_REQUEST).apply {
            resources = listOf(request)
        })
        postInquiry(inquiry)
    }

    fun updateComplaint(oldInquiry: Int, newComplaint: Complaint) {
        val inquiry = Inquiry(InquiryBuilder(InquiryType.COMPLAINT).apply {
            requestText = newComplaint.requestText
            responseText = newComplaint.responseText
            isApproved = newComplaint.isApproved
            isProcessed = newComplaint.isProcessed
        })
        updateInquiry(oldInquiry, inquiry)
    }

    fun updateRemovalRequest(oldInquiry: Int, newRemovalRequest: RemovalRequest) {
        val inquiry = Inquiry(InquiryBuilder(InquiryType.REMOVAL_REQUEST).apply {
            responseText = newRemovalRequest.responseText
            isApproved = newRemovalRequest.isApproved
            isProcessed = newRemovalRequest.isProcessed
        })
        updateInquiry(oldInquiry, inquiry)
    }

    fun updateResourceRequest(oldInquiry: Int, request: ResourceRequest) {
        val inquiry = Inquiry(InquiryBuilder(InquiryType.RESOURCE_REQUEST).apply {
            responseText = request.responseText
            isApproved = request.isApproved
            isProcessed = request.isProcessed
        })
        updateInquiry(oldInquiry, inquiry)
    }

    /**
     * ProductsApiProvider
     */

    fun getProductWithName(name: String) {
        handleSingleResponseResult(productsApi.getProductWithName(name, authorization), ResultType.GET_PRODUCT)
    }

    fun updateProduct(product: Product) {
        handleSingleResponseResult(productsApi.updateProductWithId(product.id, product, authorization), ResultType.UPDATE_PRODUCT)
    }

    @SuppressLint("CheckResult")
    fun getAllProducts() {
        notifyApiCallRequested()
        productsApi
                .getAllProducts(authorization)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val result = Result(ResultType.ALL_PRODUCTS, it)
                    resultSubscribers.forEach {
                        subscriber -> subscriber.apiResultChanged(result)
                    }
                }
    }

    /**
     * DisposalPlaceApiProvider
     */

    fun getDisposalPlaces() {
        notifyApiCallRequested()
        disposalPlaceApi.getDisposalPlaces(authorization)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<List<DisposalPlace>> {
                    override fun onSuccess(value: List<DisposalPlace>) {
                        val result = Result(ResultType.GET_DISPOSAL_PLACES, value)
                        resultSubscribers.forEach { subscriber ->
                            subscriber.apiResultChanged(result)
                        }
                    }

                    override fun onSubscribe(d: Disposable?) {
                        //ignore
                    }

                    override fun onError(e: Throwable?) {
                        val message = ErrorParser.getErrorMessage(e)
                        errorSubscribers.forEach { subscriber -> subscriber.apiErrorChanged(message) }
                    }
                })
    }

    fun updateDisposalPlace(disposalPlace: DisposalPlace) {
        handleSingleResponseResult(disposalPlaceApi.updateDisposalPlaceWithId(disposalPlace.id, disposalPlace, authorization), ResultType.UPDATE_DISPOSAL_PLACE)
    }

    /**
     * RemovalsApiProvider
     */

    fun getAllRemovals() {
        notifyApiCallRequested()
        val disp = removalsApi
                .getAllRemovalsForEachBlock(authorization)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val result = Result(ResultType.GET_ALL_REMOVALS, it)
                    resultSubscribers.forEach { subscriber -> subscriber.apiResultChanged(result) }
                }
    }

    fun getCompanyInfo() {
        handleSingleResponseResult(userApi.getCompanyInfo(authorization), ResultType.GET_COMPANY_INFO)
    }

    fun getRemovalsFor(cityBlock: String) {
        notifyApiCallRequested()
        val disp = removalsApi
                .getAllRemovals(cityBlock, authorization)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val result = Result(ResultType.GET_REMOVALS_FOR_BLOCK, it)
                    resultSubscribers.forEach { subscriber -> subscriber.apiResultChanged(result) }
                }
    }

    fun updateRemovalTime(oldRemoval: Removal, newTime: String) {
        val id = oldRemoval.id
        val newRemoval = Removal(time = newTime, id = id, cityBlock = oldRemoval.cityBlock, wasteCategory = oldRemoval.wasteCategory)
        handleSingleResponseResult(removalsApi.updateRemoval(id!!, newRemoval, authorization), ResultType.UPDATE_REMOVAL_TIME)
    }


    private fun updateInquiry(oldInquiry: Int, newInquiry: Inquiry) {
        handleSingleResponseResult(inquiriesApi.updateInquiry(oldInquiry, newInquiry, authorization), ResultType.UPDATE_INQUIRY)
    }

    private fun postInquiry(inquiry: Inquiry) {
        handleSingleResponseResult(inquiriesApi.postInquiry(inquiry, authorization), ResultType.POST_INQUIRY)
    }

    private fun handleSingleResponseResult(apiCall: Single<out Resultable>, type: ResultType) {
        notifyApiCallRequested()
        apiCall.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<Resultable> {
                    override fun onSuccess(value: Resultable?) {
                        val result = Result(type, value)
                        resultSubscribers.forEach { subscriber ->
                            subscriber.apiResultChanged(result)
                        }
                    }

                    override fun onSubscribe(d: Disposable?) {
                        //ignore
                    }

                    override fun onError(e: Throwable?) {
                        val message = ErrorParser.getErrorMessage(e)
                        errorSubscribers.forEach { subscriber -> subscriber.apiErrorChanged(message) }
                    }
                })
    }


}