package com.example.android.api

import com.example.android.models.Removal
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

interface RemovalsApi {

    @GET("/removals")
    fun getAllRemovalsForEachBlock(@Header("Authorization") auth: String): Observable<List<Removal>>

    @GET("/removals/{cityBlock}")
    fun getAllRemovals(@Path("cityBlock") cityBlock: String,
                       @Header("Authorization") auth: String): Observable<List<Removal>>

    @PUT("/removals/{id}")
    fun updateRemoval(@Path("id") id: Long,
                           @Body removal: Removal,
                           @Header("Authorization") auth: String): Single<Removal>

}