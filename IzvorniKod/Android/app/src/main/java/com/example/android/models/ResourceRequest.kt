package com.example.android.models

import com.example.android.enums.ResourceType
import com.example.android.enums.WasteCategory

data class ResourceRequest(
        val id: Long?,
        val responseText: String?,
        val quantity: Int?,
        val wasteCategory: WasteCategory?,
        val resourceType: ResourceType?,
        var isApproved: Boolean?,
        var isProcessed: Boolean?,
        var timestamp: String?
){
    companion object{
        fun createRequest(quantity: Int?, wasteCategory: WasteCategory?, resourceType: ResourceType?) =
                ResourceRequest(null, null, quantity, wasteCategory, resourceType, null, null, null)
        fun createResponse(responseText: String?, isApproved: Boolean?, isProcessed: Boolean?) =
                ResourceRequest(null, responseText,null, null,null, isApproved,isProcessed, null)
    }
}