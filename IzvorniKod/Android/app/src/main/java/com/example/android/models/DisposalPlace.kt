package com.example.android.models

import com.example.android.enums.WasteCategory
import com.example.android.interfaces.Resultable

data class DisposalPlace(
    val id: Long,
    val address: String,
    val workingHours: String,
    val wasteCategories: List<WasteCategory>
): Resultable