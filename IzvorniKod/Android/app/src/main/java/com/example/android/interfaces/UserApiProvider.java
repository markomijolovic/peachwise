package com.example.android.interfaces;

import com.example.android.models.User;

public interface UserApiProvider {
    void getAllUsers();
    void getUserWithUsername(String username);
    void updateUserWithUsername(String username, User newUser);
    void deleteUserWithUsername(String username);
    void login();
    void registerUser(User user);
}
