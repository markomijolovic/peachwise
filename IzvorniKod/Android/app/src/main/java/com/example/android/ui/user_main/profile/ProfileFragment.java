package com.example.android.ui.user_main.profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.android.R;
import com.example.android.constants.Constants;
import com.example.android.models.Result;
import com.example.android.models.User;
import com.example.android.models.UserBuilder;
import com.example.android.repository.AppRepository;
import com.example.android.ui.BaseFragment;
import com.example.android.ui.registration.RegistrationFlowActivity;
import com.example.android.utils.UserStorage;


public class ProfileFragment extends BaseFragment {

    private EditText firstName;
    private EditText lastName;
    private EditText address;
    private EditText username;
    private EditText email;
    private EditText password;
    private Button updateInfo;
    private Button deleteAccount;

    private AppRepository repository;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        repository = AppRepository.Companion.getInstance();
        initializeViews();
        populateEditTexts();
        updateInfo.setOnClickListener(v -> updateAccountInformation());
        deleteAccount.setOnClickListener(v -> deleteAccount());
    }

    private void deleteAccount() {
        String currentUsername = UserStorage.INSTANCE.getCurrentUser().getUsername();
        repository.deleteUserWithUsername(currentUsername);
    }

    private void updateAccountInformation() {
        String currentUsername = UserStorage.INSTANCE.getCurrentUser().getUsername();
        User updatedUser = getUpdatedUser();
        repository.updateUserWithUsername(currentUsername, updatedUser);
    }

    @Override
    public void apiResultChanged(Result result) {
        switch (result.getResultType()) {
            case DELETE_USER: {
                forgetUser();
                Toast.makeText(getActivity(), "Racun uspjesno obrisan!", Toast.LENGTH_SHORT).show();
                transitionToLogin();
                break;
            }
            case UPDATE_USER: {
                forgetUser();
                Toast.makeText(getActivity(), "Podaci uspjesno azurirani!\nMolimo prijavite se ponovno.", Toast.LENGTH_SHORT).show();
                transitionToLogin();
                break;
            }
        }
    }

    private void transitionToLogin() {
        Intent intent = new Intent(getActivity(), RegistrationFlowActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private User getUpdatedUser() {
        return new UserBuilder(
                        username.getText().toString(),
                        password.getText().toString())
                        .withAddress(address.getText().toString())
                        .withEmail(email.getText().toString())
                        .withFirstName(firstName.getText().toString())
                        .withLastName(lastName.getText().toString())
                        .build();


    }

    private void forgetUser() {
        final SharedPreferences sharedPref = getActivity().getSharedPreferences(Constants.PREFS, Context.MODE_PRIVATE);
        if(sharedPref == null) return;
        sharedPref
                .edit()
                .remove(Constants.REMEMBER_ME)
                .apply();

    }

    private void populateEditTexts() {
        User currentUser = UserStorage.INSTANCE.getCurrentUser();

        firstName.setText(currentUser.getFirstName());
        lastName.setText(currentUser.getLastName());
        address.setText(currentUser.getAddress());
        username.setText(currentUser.getUsername());
        email.setText(currentUser.getEmail());
    }

    private void initializeViews() {
        firstName = getActivity().findViewById(R.id.et_firstName);
        lastName = getActivity().findViewById(R.id.et_lastName);
        address = getActivity().findViewById(R.id.et_address);
        username = getActivity().findViewById(R.id.et_username);
        email = getActivity().findViewById(R.id.et_email);
        password = getActivity().findViewById(R.id.et_password);
        updateInfo = getActivity().findViewById(R.id.btn_updateInfo);
        deleteAccount = getActivity().findViewById(R.id.btn_deleteAcc);
        progressBar = getActivity().findViewById(R.id.profilePB); //inherited from base fragment
    }


}