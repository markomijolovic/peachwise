package com.example.android.models

import com.example.android.interfaces.Resultable

enum class InquiryType {
    COMPLAINT, REMOVAL_REQUEST, RESOURCE_REQUEST
}

data class Inquiry(
        var id: Int? = null,
        val inquiryType: InquiryType,
        var approved: Boolean? = null,
        var processed: Boolean? = null,
        val requestText: String? = null,
        val responseText: String? = null,
        var timestamp: String? = null,
        var personId: Int? = null,
        val resources: List<ResourceRequest>? = null,
        val requestItems: List<RemovalRequest>? = null
): Resultable {
    constructor(builder: InquiryBuilder) : this(
            inquiryType = builder.inquiryType,
            resources = builder.resources,
            requestText = builder.requestText,
            requestItems = builder.requestItems,
            responseText = builder.responseText,
            approved = builder.isApproved,
            processed = builder.isProcessed)
}

class InquiryBuilder(val inquiryType: InquiryType) {
    var requestText: String? = null
    var resources: List<ResourceRequest>? = null
    var requestItems: List<RemovalRequest>? = null
    var responseText: String? = null
    var isApproved: Boolean? = null
    var isProcessed: Boolean? = null

    fun build() = Inquiry(this)
}