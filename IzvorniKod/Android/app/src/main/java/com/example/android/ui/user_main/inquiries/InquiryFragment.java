package com.example.android.ui.user_main.inquiries;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.R;
import com.example.android.interfaces.Resultable;
import com.example.android.models.Inquiry;
import com.example.android.models.InquiryType;
import com.example.android.models.Result;
import com.example.android.models.Role;
import com.example.android.repository.AppRepository;
import com.example.android.ui.BaseFragment;
import com.example.android.ui.user_main.RespondPressedListener;
import com.example.android.ui.user_main.inquiries.adapters.EmployeeComplaintAdapter;
import com.example.android.ui.user_main.inquiries.adapters.EmployeeRemovalRequestAdapter;
import com.example.android.ui.user_main.inquiries.adapters.EmployeeResourceRequestAdapter;
import com.example.android.ui.user_main.inquiries.adapters.UserComplaintAdapter;
import com.example.android.ui.user_main.inquiries.adapters.UserRemovalRequestAdapter;
import com.example.android.ui.user_main.inquiries.adapters.UserResourceRequestAdapter;
import com.example.android.utils.UserStorage;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@SuppressWarnings("ConstantConditions")
public class InquiryFragment extends BaseFragment implements AdapterView.OnItemSelectedListener, RespondPressedListener {

    private static final Predicate<Inquiry> complaintFilter = inquiry -> inquiry.getInquiryType().equals(InquiryType.COMPLAINT);
    private static final Predicate<Inquiry> removalRequestFilter = inquiry -> inquiry.getInquiryType().equals(InquiryType.REMOVAL_REQUEST);
    private static final Predicate<Inquiry> resourceRequestFilter = inquiry -> inquiry.getInquiryType().equals(InquiryType.RESOURCE_REQUEST);

    private boolean isEmployee = UserStorage.INSTANCE.getCurrentUser().getRole() == Role.ROLE_EMPLOYEE;

    private RecyclerView inquiryRecyclerView;

    private UserComplaintAdapter userComplaintAdapter;
    private UserRemovalRequestAdapter userRemovalRequestAdapter;
    private UserResourceRequestAdapter userResourceRequestAdapter;
    private EmployeeComplaintAdapter employeeComplaintAdapter;
    private EmployeeRemovalRequestAdapter employeeRemovalRequestAdapter;
    private EmployeeResourceRequestAdapter employeeResourceRequestAdapter;

    private AppRepository repository = AppRepository.Companion.getInstance();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_inquiries, container, false);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initializeSpinner();
        initializeRecyclerViewAdapters();
        initializeRecyclerView();

        repository.getAllInquiries();

        FloatingActionButton fab = getActivity().findViewById(R.id.fab_new_inquiry);
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        fab.setOnClickListener(view -> navController.navigate(R.id.transition_to_new_inquiry));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void apiResultChanged(Result result) {

        switch (result.getResultType()) {
            case ALL_INQUIRIES: {
                List<Inquiry> inquiryList = (List<Inquiry>) result.getListResult();

                if(isEmployee) {
                    employeeComplaintAdapter.updateInquiryList(filterList(inquiryList, complaintFilter));
                    employeeRemovalRequestAdapter.updateInquiryList(filterList(inquiryList, removalRequestFilter));
                    employeeResourceRequestAdapter.updateInquiryList(filterList(inquiryList, resourceRequestFilter));
                } else {
                    userComplaintAdapter.updateInquiryList(filterList(inquiryList, complaintFilter));
                    userRemovalRequestAdapter.updateInquiryList(filterList(inquiryList, removalRequestFilter));
                    userResourceRequestAdapter.updateInquiryList(filterList(inquiryList, resourceRequestFilter));
                }

                //add fot other types of inquiries
                break;
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        switch (position) {
            case 0: {
                if(isEmployee) {
                    inquiryRecyclerView.setAdapter(employeeComplaintAdapter);
                } else {
                    inquiryRecyclerView.setAdapter(userComplaintAdapter);
                }
                break;
            }
            case 1: {
                if(isEmployee) {
                    inquiryRecyclerView.setAdapter(employeeRemovalRequestAdapter);
                } else {
                    inquiryRecyclerView.setAdapter(userRemovalRequestAdapter);
                }
                break;
            }
            case 2: {
                if(isEmployee) {
                    inquiryRecyclerView.setAdapter(employeeResourceRequestAdapter);
                } else {
                    inquiryRecyclerView.setAdapter(userResourceRequestAdapter);
                }
                break;
            }
        }
    }

    private void initializeRecyclerView() {
        inquiryRecyclerView = getActivity().findViewById(R.id.inquiry_recyclerview);
        inquiryRecyclerView.setAdapter(userComplaintAdapter);
        inquiryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void initializeRecyclerViewAdapters() {
        userComplaintAdapter = new UserComplaintAdapter(getActivity());
        userRemovalRequestAdapter = new UserRemovalRequestAdapter(getActivity());
        userResourceRequestAdapter = new UserResourceRequestAdapter(getActivity());

        employeeComplaintAdapter = new EmployeeComplaintAdapter(getActivity(), this);
        employeeRemovalRequestAdapter = new EmployeeRemovalRequestAdapter(getActivity(), this);
        employeeResourceRequestAdapter = new EmployeeResourceRequestAdapter(getActivity(), this);
    }

    private void initializeSpinner() {
        Spinner spinner = getActivity().findViewById(R.id.inquiry_filter_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.inquiry_filter, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }

    private List<Inquiry> filterList(List<Inquiry> inquiryList, Predicate<Inquiry> filter) {
        return inquiryList.stream().filter(filter).collect(Collectors.toList());
    }



    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        //ignore
    }

    @Override
    public void onRespondPressed(int destination, Resultable resultable) {
        Gson gson = new Gson();
        String json = gson.toJson(resultable);
        Bundle bundle = new Bundle();
        bundle.putString("extras", json);

        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        navController.navigate(destination, bundle);
    }
}