package com.example.android.ui.user_main.products;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.R;
import com.example.android.enums.WasteCategory;
import com.example.android.models.DisposalType;
import com.example.android.models.Product;
import com.example.android.models.Role;
import com.example.android.ui.user_main.EditPressedListener;
import com.example.android.ui.user_main.disposalPlaces.DisposalPlaceAdapter;
import com.example.android.utils.DisposalTypeProvider;
import com.example.android.utils.UserStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder> {

    private List<Product> products = new ArrayList<>();
    private Context context;
    private Map<WasteCategory, DisposalType> disposalMap = DisposalTypeProvider.INSTANCE.getDisposalTypeMap();
    private EditPressedListener editPressedListener;

    ProductsAdapter(Context ct, EditPressedListener editPressedListener) {
        context = ct;
        this.editPressedListener = editPressedListener;
    }

    void updateProductList(List<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ProductsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.products_row, parent, false);
        return new ProductsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsViewHolder holder, int position) {
        DisposalType disposalType = disposalMap.get(products.get(position).getWasteCategory());

        enableEditingIfEmployee(holder, position);

        holder.productName.setText(products.get(position).getName());
        holder.wasteCategory.setText(disposalType.getLabel());
        holder.wasteIcon.setImageResource(disposalType.getResourceId());
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    class ProductsViewHolder extends RecyclerView.ViewHolder {

        TextView productName;
        TextView wasteCategory;
        ImageView wasteIcon;
        View edit;

        ProductsViewHolder(@NonNull View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.product_name);
            wasteCategory = itemView.findViewById(R.id.waste_category);
            wasteIcon = itemView.findViewById(R.id.iv_wasteIcon);
            edit = itemView.findViewById(R.id.btn_edit);
        }
    }

    private void enableEditingIfEmployee(ProductsViewHolder holder, int position) {
        boolean isEmployee = UserStorage.INSTANCE.getCurrentUser().getRole() == Role.ROLE_EMPLOYEE;
        if(isEmployee) {
            holder.edit.setVisibility(View.VISIBLE);
            holder.edit.setOnClickListener(v -> editPressedListener.onEditPressed(products.get(position)));
        }
    }

}
