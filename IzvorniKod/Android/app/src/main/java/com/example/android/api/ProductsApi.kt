package com.example.android.api

import com.example.android.models.Product
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

interface ProductsApi {

    @GET("/products")
    fun getAllProducts(@Header("Authorization")auth: String): Observable<List<Product>>

    @GET("/products/{productName}")
    fun getProductWithName(@Path("productName") productName: String,
                           @Header("Authorization") auth: String): Single<Product>

    @PUT("/products/{id}")
    fun updateProductWithId(@Path("id") id: Long,
                            @Body product: Product,
                            @Header("Authorization") auth: String): Single<Product>
}