package com.example.android.ui.user_main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.android.R;
import com.example.android.constants.Constants;
import com.example.android.models.Role;
import com.example.android.models.User;
import com.example.android.ui.BaseActivity;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.android.ui.registration.RegistrationFlowActivity;
import com.example.android.utils.UserStorage;
import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.ViewParent;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivityUser extends BaseActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_user);
        Toolbar toolbar = findViewById(R.id.toolbar);

        progressBar = findViewById(R.id.progressBarMain);

        setSupportActionBar(toolbar);

        NavigationView navigationView;
        boolean isEmployee = UserStorage.INSTANCE.getCurrentUser().getRole() == Role.ROLE_EMPLOYEE;
        if(isEmployee) {
            navigationView = buildEmployeeDrawer();
        } else {
            navigationView = buildUserDrawer();
        }

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_logout:{
                        forgetUser();
                        Intent intent = new Intent(MainActivityUser.this, RegistrationFlowActivity.class);
                        startActivity(intent);
                        MainActivityUser.this.finish();
                        break;
                    }

                }
                boolean handled = NavigationUI.onNavDestinationSelected(item, navController);
                if (handled) {
                    ViewParent parent = navigationView.getParent();
                    if (parent instanceof DrawerLayout) {
                        ((DrawerLayout) parent).closeDrawer(navigationView);
                    }
                }

                return handled;
            }
        });
        setupHeader();

    }

    private NavigationView buildUserDrawer() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_requests, R.id.nav_products,
                R.id.nav_garbage, R.id.nav_profile)
                .setDrawerLayout(drawer)
                .build();
        navigationView.inflateMenu(R.menu.activity_main_user_drawer);
        return navigationView;
    }

    private NavigationView buildEmployeeDrawer() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_requests, R.id.nav_products,
                R.id.nav_garbage)
                .setDrawerLayout(drawer)
                .build();
        navigationView.inflateMenu(R.menu.activity_main_employee_drawer);
        return navigationView;
    }

    private void setupHeader() {
        final int usernameIndex = 1;
        final int emailIndex = 2;

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        LinearLayout linearLayout =  (LinearLayout)navigationView.getHeaderView(0);
        TextView usernameTv = (TextView)linearLayout.getChildAt(usernameIndex);
        TextView emailTv = (TextView) linearLayout.getChildAt(emailIndex);

        User user = UserStorage.INSTANCE.getCurrentUser();
        usernameTv.setText(user.getUsername());
        emailTv.setText(user.getEmail());
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void forgetUser() {
        final SharedPreferences sharedPref = getSharedPreferences(Constants.PREFS, Context.MODE_PRIVATE);
        if(sharedPref == null) return;
        sharedPref
                .edit()
                .remove(Constants.REMEMBER_ME)
                .apply();

    }
}
