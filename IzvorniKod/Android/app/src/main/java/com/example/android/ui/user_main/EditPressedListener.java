package com.example.android.ui.user_main;

import com.example.android.interfaces.Resultable;

public interface EditPressedListener {
    void onEditPressed(Resultable pressedInfo);
}
