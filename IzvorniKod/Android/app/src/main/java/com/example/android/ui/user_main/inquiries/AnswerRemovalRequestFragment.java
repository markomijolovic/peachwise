package com.example.android.ui.user_main.inquiries;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.android.R;
import com.example.android.models.Result;
import com.example.android.ui.BaseFragment;

//TODO: Jaksa -> napraviti layout i logiku, zaposlenik ovdje moze samo mijenjati vrijeme odvoza, pa adekvatno tome napraviti sve
public class AnswerRemovalRequestFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_answer_removal_request, container, false);
    }

    //ovdje je najbolje inicijalizirat viowe, ne treba ti onaj root. sranje nego getActivity()
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void apiResultChanged(Result result) {

    }
}
