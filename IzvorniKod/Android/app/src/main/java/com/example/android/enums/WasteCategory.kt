package com.example.android.enums

enum class WasteCategory {
    MIXED_COMUNAL, BIOWASTE, PAPER_CARDBOARD, PLASTIC_METAL, BULKY, TEXTILE, DANGEROUS
}