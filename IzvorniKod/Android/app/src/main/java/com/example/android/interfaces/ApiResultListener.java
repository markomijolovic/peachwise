package com.example.android.interfaces;

import com.example.android.models.Result;

public interface ApiResultListener {
    void apiResultRequested();
    void apiResultChanged(Result result);
}
