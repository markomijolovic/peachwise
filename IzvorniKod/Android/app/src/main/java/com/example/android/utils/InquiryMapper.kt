package com.example.android.utils

import com.example.android.models.*

object InquiryMapper {

    fun mapInquiriesToComplaint(inquiries: List<Inquiry>): List<Complaint> {
        val complaintList = arrayListOf<Complaint>()
        for(inquiry in inquiries) {
            if(inquiry.inquiryType == InquiryType.COMPLAINT) {
                complaintList.add(Complaint(inquiry.id, inquiry.approved, inquiry.processed,
                        inquiry.requestText, inquiry.responseText, inquiry.timestamp))
            }
        }
        return complaintList
    }

    fun mapInquiriesToRemovalRequest(inquiries: List<Inquiry>): List<RemovalRequest> {
        val removalRequestList = arrayListOf<RemovalRequest>()
        for(inquiry in inquiries) {
            if(inquiry.inquiryType == InquiryType.REMOVAL_REQUEST) {
                removalRequestList.addAll(inquiry.requestItems!!)
            }
        }
        return removalRequestList
    }

    fun mapInquiriesToResourceRequest(inquiries: List<Inquiry>): List<ResourceRequest> {
        val removalRequestList = arrayListOf<ResourceRequest>()
        for(inquiry in inquiries) {
            if(inquiry.inquiryType == InquiryType.RESOURCE_REQUEST) {
                removalRequestList.addAll(inquiry.resources!!)
            }
        }
        return removalRequestList
    }

}