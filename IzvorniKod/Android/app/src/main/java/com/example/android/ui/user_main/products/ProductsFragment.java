package com.example.android.ui.user_main.products;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.R;
import com.example.android.interfaces.Resultable;
import com.example.android.models.Product;
import com.example.android.models.Result;
import com.example.android.repository.AppRepository;
import com.example.android.ui.BaseFragment;
import com.example.android.ui.user_main.EditPressedListener;
import com.jakewharton.rxbinding2.widget.RxTextView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;


public class ProductsFragment extends BaseFragment implements EditPressedListener {

    private RecyclerView recyclerView;
    private ProductsAdapter productsAdapter;
    private List<Product> productList = new ArrayList<>();
    private EditText editTextProductSearch;
    private TextView message;

    @SuppressLint("ClickableViewAccessibility")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_products, container, false);
        recyclerView = root.findViewById(R.id.products_recyclerview);
        final AppRepository repository = AppRepository.Companion.getInstance();
        editTextProductSearch = root.findViewById(R.id.productSearch);
        message = root.findViewById(R.id.tv_message);

        initializeAdapter();
        initializeSearchField();

        repository.getAllProducts();

        return root;
    }

    private void initializeSearchField() {
        Disposable d = RxTextView.textChanges(editTextProductSearch)
                .skipInitialValue()
                .debounce(200, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(text -> {
                    List<Product> list = productList.stream()
                            .filter(product -> product.getName().toLowerCase().startsWith(text.toString().toLowerCase()))
                            .collect(Collectors.toList());
                    productsAdapter.updateProductList(list);
                    if(list.isEmpty()) {
                        message.setVisibility(View.VISIBLE);
                    } else {
                        message.setVisibility(View.GONE);
                    }
                });
    }

    private void initializeAdapter() {
        productsAdapter = new ProductsAdapter(getActivity(), this);
        recyclerView.setAdapter(productsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void apiResultChanged(@NotNull Result result) {
        switch (result.getResultType()) {
            case ALL_PRODUCTS: {
                productList = (List<Product>) result.getListResult();
                productsAdapter.updateProductList(productList);
            }
        }

    }

    @Override
    public void onEditPressed(Resultable pressedInfo) {
        Product pressedProduct = (Product) pressedInfo;
    
        Bundle bundle = new Bundle();
        bundle.putString("productName", ((Product) pressedInfo).getName());
        bundle.putString("wasteCategory", ((Product) pressedInfo).getWasteCategory().toString());
        bundle.putLong("productId", ((Product) pressedInfo).getId());
        final NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        navController.navigate(R.id.action_nav_products_to_updateProductFragment, bundle);
    }
}