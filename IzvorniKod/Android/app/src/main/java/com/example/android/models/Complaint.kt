package com.example.android.models

data class Complaint(
        val id: Int?,
        val isApproved: Boolean?,
        val isProcessed: Boolean?,
        val requestText: String?,
        val responseText: String?,
        val timestamp: String?
){
    companion object {
        fun createRequest(requestText: String?) = Complaint(null, null, null, requestText,null, null)
        fun createResponse(responseText: String?, isApproved: Boolean?, isProcessed: Boolean?) = Complaint(null, isApproved, isProcessed, null, responseText, null)
    }
}