package com.example.android.models

import com.example.android.interfaces.Resultable

enum class Role {
    ROLE_ADMIN, ROLE_EMPLOYEE, ROLE_USER
}

data class User (
        val id: Int?,
        val username: String,
        val address: String?,
        val email: String?,
        val firstName: String?,
        val lastName: String?,
        val password: String,
        val role: Role
): Resultable {
    constructor(builder: UserBuilder) : this(
            builder.id, builder.username, builder.address,
            builder.email, builder.firstName, builder.lastName,
            builder.password, builder.role)
}

class UserBuilder(val username: String, val password: String) {
    var role: Role = Role.ROLE_USER
    var id: Int? = null
    var address: String? = null
    var email: String? = null
    var firstName: String? = null
    var lastName: String? = null

    fun withAddress(address: String): UserBuilder {
        this.address = address
        return this
    }
    fun withEmail(email: String): UserBuilder {
        this.email = email
        return this
    }
    fun withFirstName(firstName: String): UserBuilder {
        this.firstName = firstName
        return this
    }
    fun withLastName(lastName: String): UserBuilder {
        this.lastName = lastName
        return this
    }

    fun build() = User(this)
}