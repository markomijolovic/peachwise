package com.example.android.api

import com.example.android.models.DisposalPlace
import com.example.android.models.Product
import io.reactivex.Single
import retrofit2.http.*

interface DisposalPlaceApi {

    @GET("/disposal_places")
    fun getDisposalPlaces(@Header("Authorization") auth: String): Single<List<DisposalPlace>>

    @PUT("/disposal_places/{id}")
    fun updateDisposalPlaceWithId(@Path("id") id: Long,
                                  @Body disposalPlace: DisposalPlace,
                                  @Header("Authorization") auth: String): Single<DisposalPlace>
}