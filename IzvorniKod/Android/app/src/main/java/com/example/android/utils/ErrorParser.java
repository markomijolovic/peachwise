package com.example.android.utils;

import com.example.android.models.BadRequest;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.Objects;

import retrofit2.HttpException;

public class ErrorParser {

    public static String getErrorMessage(Throwable e) {
        try {
            Gson gson = new Gson();
            BadRequest badRequest = gson.fromJson(Objects.requireNonNull(
                    Objects.requireNonNull(
                            ((HttpException) e).response()).errorBody()).string(), BadRequest.class);
            // TODO: Fix this... maybe
            if (badRequest.getMessage().equals("Unauthorized"))
            {
                return "Neispravno korisničko ime i/ili lozinka.";
            }
            return badRequest.getMessage();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return "Nesto je poslo po zlu.\nProvjerite svoju internet vezu.";
    }

}
