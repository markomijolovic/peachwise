package com.example.android.ui.user_main.schedule;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.android.R;
import com.example.android.interfaces.Resultable;
import com.example.android.models.Removal;
import com.example.android.models.Result;
import com.example.android.repository.AppRepository;
import com.example.android.ui.BaseFragment;
import com.example.android.ui.user_main.EditPressedListener;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;


public class ScheduleFragment extends BaseFragment implements EditPressedListener {

    private RecyclerView recyclerView;
    private ScheduleAdapter scheduleAdapter;
    private EditText editTextScheduleSearch;
    private List<Removal> removalList = new ArrayList<>();
    private TextView message;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_schedule, container, false);

        recyclerView = root.findViewById(R.id.schedule_recyclerview);
        editTextScheduleSearch = root.findViewById(R.id.et_findSchedule);
        message = root.findViewById(R.id.tv_message);

        initializeAdapter();
        final AppRepository repository = AppRepository.Companion.getInstance();
        repository.getAllRemovals();
        initializeSearchField();

        return root;
    }

    private void initializeAdapter() {
        scheduleAdapter = new ScheduleAdapter(getActivity(), this);
        recyclerView.setAdapter(scheduleAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void initializeSearchField() {
        Disposable d = RxTextView.textChanges(editTextScheduleSearch)
                .skipInitialValue()
                .debounce(200, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(text -> {
                    List<Removal> list = removalList.stream()
                            .filter(removal -> removal.getCityBlock().toLowerCase().startsWith(text.toString().toLowerCase()))
                            .collect(Collectors.toList());
                    scheduleAdapter.updateRemovalList(list);
                    if(list.isEmpty()) {
                        message.setVisibility(View.VISIBLE);
                    } else {
                        message.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void apiResultChanged(Result result) {
        switch (result.getResultType()) {
            case GET_ALL_REMOVALS:
                removalList = (List<Removal>) result.getListResult();
                scheduleAdapter.updateRemovalList(removalList);
                break;
        }

    }

    @Override
    public void onEditPressed(Resultable pressedInfo) {
        Removal removal = (Removal) pressedInfo;
        Bundle bundle = new Bundle();
        bundle.putString("block", ((Removal) pressedInfo).getCityBlock());
        bundle.putString("time", ((Removal) pressedInfo).getTime());
        bundle.putLong("id", ((Removal) pressedInfo).getId());
        bundle.putString("category", ((Removal) pressedInfo).getWasteCategory().toString());
        final NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        navController.navigate(R.id.action_nav_garbage_to_updateScheduleFragment, bundle);
    }
}