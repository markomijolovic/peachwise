package com.example.android.ui.user_main.inquiries.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.R;
import com.example.android.models.Inquiry;

import java.util.ArrayList;
import java.util.List;

public class UserComplaintAdapter extends RecyclerView.Adapter<UserComplaintAdapter.InquiryViewHolder> {

    private List<Inquiry> complaintList = new ArrayList<>();
    private Context context;

    public UserComplaintAdapter(Context context) {
        this.context = context;
    }

    public void updateInquiryList(List<Inquiry> inquiryList) {
        this.complaintList = inquiryList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public InquiryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.complaint_user_item, parent, false);
        return new InquiryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InquiryViewHolder holder, int position) {
        Inquiry complaint = complaintList.get(position);
        holder.complaintText.setText(complaint.getRequestText());
        holder.complaintResponse.setText(complaint.getResponseText() != null ? complaint.getResponseText() : "Nema odgovora");
        holder.isProcessed.setChecked(complaint.getProcessed() != null ? complaint.getProcessed() : false);
        holder.isApproved.setChecked(complaint.getApproved() != null ? complaint.getApproved() : false);
        holder.complaintDate.setText(formatDate(complaint.getTimestamp()));
    }

    @Override
    public int getItemCount() {
        return complaintList.size();
    }

    class InquiryViewHolder extends RecyclerView.ViewHolder {

        private final TextView complaintText;
        private final CheckBox isApproved;
        private final CheckBox isProcessed;
        private final TextView complaintResponse;
        private final TextView complaintDate;

        InquiryViewHolder(@NonNull View itemView) {
            super(itemView);
            complaintText = itemView.findViewById(R.id.tv_complaint_text);
            complaintResponse = itemView.findViewById(R.id.tv_complaint_response);
            isApproved = itemView.findViewById(R.id.cb_isApproved);
            isProcessed = itemView.findViewById(R.id.cb_isProcessed);
            complaintDate = itemView.findViewById(R.id.tv_complaint_date);
        }
    }

    private String formatDate(String date) {
        String month = date.substring(5,7);
        String day = date.substring(8,10);
        String year = date.substring(0,4);
        return day + "." + month + "." + year;
    }
}
