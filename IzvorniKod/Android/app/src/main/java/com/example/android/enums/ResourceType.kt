package com.example.android.enums

enum class ResourceType {
    BUCKET, BAG, CONTAINER
}