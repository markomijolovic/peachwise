package com.example.android.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.android.R;
import com.example.android.interfaces.ApiErrorListener;
import com.example.android.interfaces.ApiResultListener;
import com.example.android.models.Result;
import com.example.android.repository.AppRepository;

import org.jetbrains.annotations.NotNull;

abstract public class BaseActivity extends AppCompatActivity implements ApiErrorListener, ApiResultListener {

    private final AppRepository repo = AppRepository.Companion.getInstance();
    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        repo.subscribeToResult(this);
        repo.subscribeToError(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        progressBar = findViewById(R.id.progressBarMain);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        repo.unsubscribeFromError(this);
    }

    //moze se nadjacat ako se zeli nesto drugo raditi sa errorom
    @Override
    public void apiErrorChanged(@NotNull String message) {
        if(progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void apiResultChanged(Result result) {
        if(progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void apiResultRequested() {
        if(progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }
}
