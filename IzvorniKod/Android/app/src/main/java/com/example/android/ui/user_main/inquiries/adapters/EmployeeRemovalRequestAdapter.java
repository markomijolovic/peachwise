package com.example.android.ui.user_main.inquiries.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.R;
import com.example.android.models.DisposalType;
import com.example.android.models.Inquiry;
import com.example.android.models.RemovalRequest;
import com.example.android.ui.user_main.RespondPressedListener;
import com.example.android.utils.DisposalTypeProvider;

import java.util.ArrayList;
import java.util.List;

public class EmployeeRemovalRequestAdapter extends RecyclerView.Adapter<EmployeeRemovalRequestAdapter.InquiryViewHolder> {

    private List<Inquiry> complaintList = new ArrayList<>();
    private Context context;
    private RespondPressedListener respondPressedListener;

    public EmployeeRemovalRequestAdapter(Context context, RespondPressedListener respondPressedListener) {
        this.context = context;
        this.respondPressedListener = respondPressedListener;
    }

    public void updateInquiryList(List<Inquiry> inquiryList) {
        this.complaintList = inquiryList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public InquiryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.removal_request_employee_item, parent, false);
        return new InquiryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InquiryViewHolder holder, int position) {
        Inquiry inquiry = complaintList.get(position);
        RemovalRequest removalRequest = inquiry.getRequestItems().get(0);
        DisposalType disposalType = DisposalTypeProvider.INSTANCE.getDisposalTypeMap().get(removalRequest.getWasteCategory());

        holder.amount.setText(removalRequest.getVolume() + " kg");
        holder.disposalType.setText(disposalType.getLabel());
        holder.complaintDate.setText(formatDate(inquiry.getTimestamp()));
        holder.button.setOnClickListener(v ->
                respondPressedListener.onRespondPressed(R.id.action_nav_requests_to_answerRemovalRequestFragment, inquiry));


    }

    @Override
    public int getItemCount() {
        return complaintList.size();
    }

    class InquiryViewHolder extends RecyclerView.ViewHolder {

        private final TextView disposalType;
        private final TextView complaintDate;
        private final TextView amount;
        private final Button button;

        InquiryViewHolder(@NonNull View itemView) {
            super(itemView);
            disposalType = itemView.findViewById(R.id.tv_disposal_type);
            complaintDate = itemView.findViewById(R.id.tv_complaint_date);
            amount = itemView.findViewById(R.id.tv_amount);
            button = itemView.findViewById(R.id.btn_answer);
        }
    }

    private String formatDate(String date) {
        String month = date.substring(5,7);
        String day = date.substring(8,10);
        String year = date.substring(0,4);
        return day + "." + month + "." + year;
    }
}
