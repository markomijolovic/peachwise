package com.example.android.models

import com.example.android.enums.WasteCategory
import com.example.android.interfaces.Resultable

data class Product(
        val id: Long,
        val name: String,
        val wasteCategory: WasteCategory
): Resultable