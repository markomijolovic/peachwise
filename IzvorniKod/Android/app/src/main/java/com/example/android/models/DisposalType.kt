package com.example.android.models

data class DisposalType (
        val label: String,
        val resourceId: Int
)
