package com.example.android.ui.registration;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.FragmentTransaction;

import com.example.android.R;
import com.example.android.constants.Constants;
import com.example.android.models.User;
import com.example.android.repository.AppRepository;
import com.example.android.ui.BaseActivity;
import com.example.android.ui.user_main.MainActivityUser;
import com.example.android.utils.UserStorage;
import com.google.gson.Gson;


public class RegistrationFlowActivity extends BaseActivity {

    private final AppRepository repo = AppRepository.Companion.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(isUserRemembered()) {
            setupUser();
            transitionToActivity();
        }
        setContentView(R.layout.activity_registration_flow);
        transitionToFragment();
    }

    private void setupUser() {
        String userJson = getUserJson();
        if(userJson == null) return;

        User user = new Gson().fromJson(userJson, User.class);
        String password = getPassword();
        UserStorage.INSTANCE.storeCurrentUser(user);
        AppRepository.Companion.getInstance().setUsernameAndPassword(user.getUsername(), password);
    }

    private String getPassword() {
        SharedPreferences sharedPref = getSharedPreferences(Constants.PREFS, Context.MODE_PRIVATE);
        if(sharedPref == null) return null;
        return sharedPref.getString(Constants.PASSWORD, null);
    }

    private String getUserJson() {
        SharedPreferences sharedPref = getSharedPreferences(Constants.PREFS, Context.MODE_PRIVATE);
        if(sharedPref == null) return null;
        return sharedPref.getString(Constants.USER, null);
    }

    private boolean isUserRemembered() {
        SharedPreferences sharedPref = getSharedPreferences(Constants.PREFS, Context.MODE_PRIVATE);
        if(sharedPref == null) return false;
        return sharedPref.getBoolean(Constants.REMEMBER_ME, false);
    }

    private void transitionToFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.rootLayout, new LoginFragment());
        transaction.commit();
    }

    private void transitionToActivity() {
        Intent intent = new Intent(this, MainActivityUser.class);
        startActivity(intent);
        finish();
    }



}
