package com.example.android.ui.user_main.disposalPlaces;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.R;
import com.example.android.enums.WasteCategory;
import com.example.android.interfaces.Resultable;
import com.example.android.models.DisposalPlace;
import com.example.android.models.Result;
import com.example.android.repository.AppRepository;
import com.example.android.ui.BaseFragment;
import com.example.android.ui.user_main.EditPressedListener;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class DisposalPlacesFragment extends BaseFragment implements EditPressedListener
{

    private DisposalPlaceAdapter disposalPlaceAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final AppRepository repository = AppRepository.Companion.getInstance();
        repository.subscribeToResult(this);
        getActivity().findViewById(R.id.progressBarMain).setVisibility(View.VISIBLE);
        repository.getDisposalPlaces();

        initializeRecyclerView();
    }

    private void initializeRecyclerView() {
        disposalPlaceAdapter = new DisposalPlaceAdapter(getActivity(), this);
        final RecyclerView recyclerView = getActivity().findViewById(R.id.home_recyclerview);
        recyclerView.setAdapter(disposalPlaceAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }


    @Override
    public void apiResultChanged(Result result) {
        switch (result.getResultType()) {
            case GET_DISPOSAL_PLACES: {
                List<DisposalPlace> disposalPlaceList = (List<DisposalPlace>) result.getListResult();
                disposalPlaceAdapter.updateDisposalPlaces(disposalPlaceList);
                break;
            }
        }

    }

    @Override
    public void onEditPressed(Resultable pressedInfo) {
        DisposalPlace disposalPlace = (DisposalPlace) pressedInfo;
        Bundle bundle = new Bundle();
        bundle.putString("time", ((DisposalPlace) pressedInfo).getWorkingHours());
        bundle.putString("address", ((DisposalPlace) pressedInfo).getAddress());
        bundle.putLong("id", ((DisposalPlace) pressedInfo).getId());
        Log.d("adresa", ((DisposalPlace) pressedInfo).getAddress());
        List<WasteCategory> list = ((DisposalPlace) pressedInfo).getWasteCategories();
        List<String> strings = list.stream().map(WasteCategory::toString)
                .collect(Collectors.toList());
        ArrayList<String> arrlistofcategories = new ArrayList<String>(strings);
        bundle.putSerializable("categories", arrlistofcategories);

        final NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        navController.navigate(R.id.action_nav_home_to_updateDisposalPlaceFragment, bundle);
    }
}

