package com.example.android.api

import com.example.android.models.Inquiry
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

interface InquiriesApi {

    @GET("/inquiries")
    fun getAllInquiries(@Header("Authorization") auth: String): Observable<List<Inquiry>>

    @POST("/inquiries")
    fun postInquiry(@Body inquiry: Inquiry,
                    @Header("Authorization") auth: String): Single<Inquiry>

    @PUT("/inquiries/{id}")
    fun updateInquiry(@Path("id") id: Int,
                      @Body inquiry: Inquiry,
                      @Header("Authorization") auth: String): Single<Inquiry>


}