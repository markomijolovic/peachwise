package com.example.android.ui.user_main.schedule;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.android.R;
import com.example.android.enums.WasteCategory;
import com.example.android.models.Removal;
import com.example.android.models.Result;
import com.example.android.repository.AppRepository;
import com.example.android.ui.BaseFragment;

//TODO: Jaksa -> napraviti layout i logiku, zaposlenik ovdje moze samo mijenjati vrijeme odvoza, pa adekvatno tome napraviti sve
public class UpdateScheduleFragment extends BaseFragment {

    AppRepository repository;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        repository = AppRepository.Companion.getInstance();
        return inflater.inflate(R.layout.fragment_update_schedule, container, false);
    }

    //ovdje je najbolje inicijalizirat viowe, ne treba ti onaj root. sranje nego getActivity()
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView block = getActivity().findViewById(R.id.update_schedule_block);
        block.setText(getArguments().getString("block"));
        EditText time = getActivity().findViewById(R.id.update_schedule_time);
        time.setText(getArguments().getString("time"));
        Button button = getActivity().findViewById(R.id.button_update_schedule);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WasteCategory category = WasteCategory.BULKY;
                if(getArguments().get("category") == WasteCategory.BULKY.toString()) category = WasteCategory.BULKY;
                else if(getArguments().get("category") == WasteCategory.BIOWASTE.toString()) category = WasteCategory.BIOWASTE;
                else if(getArguments().get("category") == WasteCategory.DANGEROUS.toString()) category = WasteCategory.DANGEROUS;
                else if(getArguments().get("category") == WasteCategory.MIXED_COMUNAL.toString()) category = WasteCategory.MIXED_COMUNAL;
                else if(getArguments().get("category") == WasteCategory.PAPER_CARDBOARD.toString()) category = WasteCategory.PAPER_CARDBOARD;
                else if(getArguments().get("category") == WasteCategory.PLASTIC_METAL.toString()) category = WasteCategory.PLASTIC_METAL;
                else if(getArguments().get("category") == WasteCategory.TEXTILE.toString()) category = WasteCategory.TEXTILE;
                Removal oldRemoval = new Removal((long)getArguments().get("id"), block.toString(), time.toString(), category);
                repository.updateRemovalTime(oldRemoval, time.getText().toString());
            }
        });
    }

    @Override
    public void apiResultChanged(Result result) {
        switch (result.getResultType()){
            case UPDATE_REMOVAL_TIME:{
                Toast.makeText(getActivity(), "Uspješna promjena vremena odvoza", Toast.LENGTH_LONG).show();
            }
        }
    }
}
