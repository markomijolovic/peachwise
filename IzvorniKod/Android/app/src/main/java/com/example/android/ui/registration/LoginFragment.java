package com.example.android.ui.registration;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.example.android.R;
import com.example.android.constants.Constants;
import com.example.android.models.Result;
import com.example.android.models.User;
import com.example.android.repository.AppRepository;
import com.example.android.ui.BaseFragment;
import com.example.android.ui.user_main.MainActivityUser;
import com.example.android.utils.UserStorage;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

public class LoginFragment extends BaseFragment {
    private AppRepository repository;
    private EditText usernameField;
    private EditText passwordField;
    private CheckBox rememberMe;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        repository = AppRepository.Companion.getInstance();
        View root =  inflater.inflate(R.layout.fragment_login, container, false);

        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        usernameField = getActivity().findViewById(R.id.et_username);
        passwordField = getActivity().findViewById(R.id.et_password);
        rememberMe = getActivity().findViewById(R.id.cb_rememberMe);
        final Button btn_reg = getActivity().findViewById(R.id.btn_Reg);
        btn_reg.setOnClickListener(v -> TransitionToFragment());
        final Button btn_login = getActivity().findViewById(R.id.btn_login);
        btn_login.setOnClickListener(v ->
                login(usernameField.getText().toString(), passwordField.getText().toString()));
    }

    @Override
    public void apiResultChanged(@NotNull Result result) {
        switch (result.getResultType()) {
            case LOGIN_USER: {
                User user = (User) result.getSingleResult();
                if(rememberMe.isChecked()) {
                    rememberUser(user);
                }
                UserStorage.INSTANCE.storeCurrentUser(user);
                transitionToMainPage();//zanima nas rezultat samo ako je on tipa LOGIN_USER, jer to vraca api kod logina
            }
        }
    }

    private void rememberUser(User user) {
        final SharedPreferences sharedPref = getActivity().getSharedPreferences(Constants.PREFS, Context.MODE_PRIVATE);
        if(sharedPref == null) return;
        final String userJson = convertUserToJson(user);
        final String password = passwordField.getText().toString();
        sharedPref
                .edit()
                .putBoolean(Constants.REMEMBER_ME, true)
                .putString(Constants.USER, userJson)
                .putString(Constants.PASSWORD, password)
                .apply();

    }

    private String convertUserToJson(User user) {
        Gson gson = new Gson();
        return gson.toJson(user);
    }

    private void login(String username, String password) {
        repository.setUsernameAndPassword(username, password);
        repository.login();
    }


    private void transitionToMainPage() {
        Intent intent = new Intent(getActivity(), MainActivityUser.class);
        startActivity(intent);
        getActivity().finish(); //ovime se ne mozemo vise vratiti na taj activity
    }

    private void TransitionToFragment() {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.rootLayout, new RegistrationFragment());
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
