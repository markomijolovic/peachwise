package com.example.android.models

import com.example.android.enums.WasteCategory

data class RemovalRequest(
        val id: Long?,
        val responseText: String?,
        val volume: Double?,
        val wasteCategory: WasteCategory?,
        val isApproved: Boolean?,
        val isProcessed: Boolean?,
        val timestamp: String?
){
    companion object {
        fun createRequest(volume: Double, wasteCategory: WasteCategory) =
                RemovalRequest(null, null, volume, wasteCategory, null, null, null)

        fun createResponse(responseText: String?, isApproved: Boolean?, isProcessed: Boolean?) =
                RemovalRequest(null, responseText, null, null, isApproved, isProcessed, null)
    }
}