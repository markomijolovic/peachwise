package com.example.android.interfaces;

public interface ApiErrorListener {
    void apiErrorChanged(String message);
}
