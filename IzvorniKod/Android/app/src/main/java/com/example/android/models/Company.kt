package com.example.android.models

import com.example.android.interfaces.Resultable

data class Company(
        val name: String,
        val address: String,
        val phoneNumber: String,
        val email: String
): Resultable