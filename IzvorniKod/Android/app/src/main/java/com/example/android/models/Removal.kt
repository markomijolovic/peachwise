package com.example.android.models

import com.example.android.enums.WasteCategory
import com.example.android.interfaces.Resultable

data class Removal(
        var id: Long? = null,
        val cityBlock: String,
        val time: String,
        val wasteCategory: WasteCategory
): Resultable