package com.example.android.ui.user_main.disposalPlaces;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.R;
import com.example.android.enums.WasteCategory;
import com.example.android.models.DisposalPlace;
import com.example.android.models.DisposalType;
import com.example.android.models.Role;
import com.example.android.models.User;
import com.example.android.ui.user_main.EditPressedListener;
import com.example.android.utils.DisposalTypeProvider;
import com.example.android.utils.UserStorage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DisposalPlaceAdapter extends RecyclerView.Adapter<DisposalPlaceAdapter.HomeViewHolder> {

    private List<DisposalPlace> disposalPlaces = new ArrayList<>();
    private Context context;
    private List<Address> addresses;
    private List<Address> address;
    private EditPressedListener editPressedListener;


    DisposalPlaceAdapter(Context ct, EditPressedListener editPressedListener) {
        context = ct;
        this.editPressedListener = editPressedListener;
    }

    public void updateDisposalPlaces(List<DisposalPlace> disposalPlaces) {
        this.disposalPlaces = disposalPlaces;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.home_row, parent, false);
        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder holder, int position) {
        DisposalPlace disposalPlace = disposalPlaces.get(position);
        
        enableEditingIfEmployee(holder, position);

        holder.hours.setText(disposalPlace.getAddress());
        holder.address.setText("Radno vrijeme: " + disposalPlace.getWorkingHours()); //ovo je zapravo radno vrijeme lol

        List<WasteCategory> wasteCategories = disposalPlace.getWasteCategories();
        List<DisposalType> disposalTypes =
                wasteCategories.stream().map( this::convertWasteCategoryToDisposalType).collect(Collectors.toList());

        DisposalTypeAdapter adapter = new DisposalTypeAdapter(context, disposalTypes);
        holder.wasteCategory.setAdapter(adapter);
        holder.map.setOnClickListener(v -> {
                Geocoder geocoder = new Geocoder(context);
                User currentUser = UserStorage.INSTANCE.getCurrentUser();
                double destinationLatitude;
                double destinationLongitude;
                double sourceLatitude;
                double sourceLongitude;
                String uri = null;

                try {
                    if(currentUser.getAddress() != null){
                        address = geocoder.getFromLocationName(currentUser.getAddress(), 1);
                        sourceLatitude = address.get(0).getLatitude();
                        sourceLongitude = address.get(0).getLongitude();
                        addresses = geocoder.getFromLocationName(disposalPlace.getAddress(), 1);
                        destinationLatitude = addresses.get(0).getLatitude();
                        destinationLongitude = addresses.get(0).getLongitude();
                        uri = "http://maps.google.com/maps?saddr=" + sourceLatitude + "," + sourceLongitude + "&daddr=" + destinationLatitude + "," + destinationLongitude;

                    } else {
                        addresses = geocoder.getFromLocationName(disposalPlace.getAddress(), 1);
                        destinationLatitude = addresses.get(0).getLatitude();
                        destinationLongitude = addresses.get(0).getLongitude();
                        uri = "http://maps.google.com/maps?daddr=" + destinationLatitude + "," + destinationLongitude;
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

                Toast.makeText(context, "Tražim: " + disposalPlace.getAddress(), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                try {
                    context.startActivity(intent);
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(context, "Instalirajte google maps ili preglednik" + disposalPlace.getAddress(), Toast.LENGTH_SHORT).show();
                }
        });

    }

    private void enableEditingIfEmployee(HomeViewHolder holder, int position) {
        boolean isEmployee = UserStorage.INSTANCE.getCurrentUser().getRole() == Role.ROLE_EMPLOYEE;
        if(isEmployee) {
            holder.edit.setVisibility(View.VISIBLE);
            holder.edit.setOnClickListener(v -> editPressedListener.onEditPressed(disposalPlaces.get(position)));
        }
    }

    private DisposalType convertWasteCategoryToDisposalType(WasteCategory wasteCategory) {
        return DisposalTypeProvider.INSTANCE.getDisposalTypeMap().get(wasteCategory);
    }

    @Override
    public int getItemCount() {
        return disposalPlaces.size();
    }

    class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView hours, address;
        private Button map;
        private GridView wasteCategory;
        private View edit;


        HomeViewHolder(@NonNull View itemView) {
            super(itemView);
            hours = itemView.findViewById(R.id.disposal_hours);
            address = itemView.findViewById(R.id.disposal_address);
            wasteCategory = itemView.findViewById(R.id.disposal_type);
            map = itemView.findViewById(R.id.buttonShowMap);
            edit = itemView.findViewById(R.id.btn_edit);
        }
    }
}
