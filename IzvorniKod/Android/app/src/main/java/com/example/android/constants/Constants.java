package com.example.android.constants;

public class Constants {

    public static final String PREFS = "prefs";

    public static final String USER = "user";
    public static final String PASSWORD = "password";
    public static final String REMEMBER_ME = "remember_me";

}
