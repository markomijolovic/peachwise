package com.example.android.ui.user_main.inquiries.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.R;
import com.example.android.models.DisposalType;
import com.example.android.models.Inquiry;
import com.example.android.models.RemovalRequest;
import com.example.android.utils.DisposalTypeProvider;

import java.util.ArrayList;
import java.util.List;

public class UserRemovalRequestAdapter extends RecyclerView.Adapter<UserRemovalRequestAdapter.InquiryViewHolder> {

    private List<Inquiry> removalREquestList = new ArrayList<>();
    private Context context;

    public UserRemovalRequestAdapter(Context context) {
        this.context = context;
    }

    public void updateInquiryList(List<Inquiry> inquiryList) {
        this.removalREquestList = inquiryList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public InquiryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.removal_request_user_item, parent, false);
        return new InquiryViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull InquiryViewHolder holder, int position) {
        Inquiry removalRequest = removalREquestList.get(position);
        RemovalRequest removalItem = removalRequest.getRequestItems().get(0);
        DisposalType disposalType = DisposalTypeProvider.INSTANCE.getDisposalTypeMap().get(removalItem.getWasteCategory());

        holder.weight.setText(removalItem.getVolume() + " kg");
        holder.wasteCategory.setText(disposalType.getLabel());

        holder.complaintResponse.setText(removalItem.getResponseText() != null ? removalItem.getResponseText() : "Nema odgovora");
        holder.isProcessed.setChecked(removalRequest.getProcessed() != null ? removalRequest.getProcessed() : false);
        holder.isApproved.setChecked(removalRequest.getApproved() != null ? removalRequest.getApproved() : false);
        holder.complaintDate.setText(formatDate(removalRequest.getTimestamp()));
    }

    @Override
    public int getItemCount() {
        return removalREquestList.size();
    }

    class InquiryViewHolder extends RecyclerView.ViewHolder {

        private final TextView wasteCategory;
        private final TextView weight;
        private final CheckBox isApproved;
        private final CheckBox isProcessed;
        private final TextView complaintResponse;
        private final TextView complaintDate;

        InquiryViewHolder(@NonNull View itemView) {
            super(itemView);
            weight = itemView.findViewById(R.id.tv_volume);
            wasteCategory = itemView.findViewById(R.id.tv_waste_category);
            complaintResponse = itemView.findViewById(R.id.tv_complaint_response);
            isApproved = itemView.findViewById(R.id.cb_isApproved);
            isProcessed = itemView.findViewById(R.id.cb_isProcessed);
            complaintDate = itemView.findViewById(R.id.tv_complaint_date);
        }
    }

    private String formatDate(String date) {
        String month = date.substring(5,7);
        String day = date.substring(8,10);
        String year = date.substring(0,4);
        return day + "." + month + "." + year;
    }
}
