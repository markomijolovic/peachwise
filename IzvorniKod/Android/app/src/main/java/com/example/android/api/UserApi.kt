package com.example.android.api

import com.example.android.models.Company
import com.example.android.models.User
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

interface UserApi {

    @POST("/register")
    fun registerUser(@Body user: User,
                     @Header("Authorization") auth: String): Single<User>

    @POST("/login")
    fun login(@Header("Authorization") auth: String): Single<User>

    @GET("/users/{username}")
    fun getUserWithUsername(@Path("username") username: String,
                            @Header("Authorization") auth: String): Single<User>

    @GET("/users")
    fun getAllUsers(@Header("Authorization") auth: String): Observable<List<User>>

    @PUT("/users/{username}")
    fun updateUserWithUsername(@Path("username") username: String,
                               @Body newUser: User,
                               @Header("Authorization") auth: String): Single<User>

    @DELETE("/users/{username}")
    fun deleteUserWithUsername(@Path("username") username: String,
                               @Header("Authorization") auth: String): Single<User>

    @GET("/company")
    fun getCompanyInfo(@Header("Authorization") auth: String): Single<Company>

}