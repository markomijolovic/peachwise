package com.example.android.ui.user_main.disposalPlaces;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.android.R;
import com.example.android.enums.WasteCategory;
import com.example.android.models.DisposalPlace;
import com.example.android.models.Result;
import com.example.android.repository.AppRepository;
import com.example.android.ui.BaseFragment;

import java.util.ArrayList;
import java.util.List;

//TODO: Jaksa -> dodaj logiku i napravi layout, zaposlenik moze samo mijenjati vrijeme odvoza i podrzane vrste otpada na tom smetlistu, pa nekako to napraviti
public class UpdateDisposalPlaceFragment extends BaseFragment {
    AppRepository repository;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        repository = AppRepository.Companion.getInstance();
        return inflater.inflate(R.layout.fragment_update_disposal_place, container, false);
    }

    //ovdje je najbolje inicijalizirat viowe, ne treba ti onaj root. sranje
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CheckedTextView bulky = getActivity().findViewById(R.id.checked_bulky);
        CheckedTextView plasticMetal = getActivity().findViewById(R.id.checked_plastic_metal);
        CheckedTextView paperCardboard = getActivity().findViewById(R.id.checked_paper_cardboard);
        CheckedTextView bioWaste = getActivity().findViewById(R.id.checked_biowaste);
        CheckedTextView mixedWaste = getActivity().findViewById(R.id.checked_mixed_waste);
        CheckedTextView dangereous = getActivity().findViewById(R.id.checked_dangereous);
        CheckedTextView textile = getActivity().findViewById(R.id.checked_textile);
        TextView address = getActivity().findViewById(R.id.update_disposal_address);
        EditText time = getActivity().findViewById(R.id.update_disposal_time);
        Button applyChanges = getActivity().findViewById((R.id.button_apply_disposal));
        address.setText(getArguments().getString("address"));
        time.setText(getArguments().getString("time"));

        bulky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bulky.isChecked()) {
                    bulky.setChecked(false);
                    bulky.setCheckMarkDrawable(null);
                } else {
                    bulky.setChecked(true);
                    bulky.setCheckMarkDrawable(R.drawable.ic_check);
                }
            }
        });
        plasticMetal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (plasticMetal.isChecked()) {
                    plasticMetal.setChecked(false);
                    plasticMetal.setCheckMarkDrawable(null);
                } else {
                    plasticMetal.setChecked(true);
                    plasticMetal.setCheckMarkDrawable(R.drawable.ic_check);
                }
            }
        });
        paperCardboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (paperCardboard.isChecked()) {
                    paperCardboard.setChecked(false);
                    paperCardboard.setCheckMarkDrawable(null);
                } else {
                    paperCardboard.setChecked(true);
                    paperCardboard.setCheckMarkDrawable(R.drawable.ic_check);
                }
            }
        });
        bioWaste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bioWaste.isChecked()) {
                    bioWaste.setChecked(false);
                    bioWaste.setCheckMarkDrawable(null);
                } else {
                    bioWaste.setChecked(true);
                    bioWaste.setCheckMarkDrawable(R.drawable.ic_check);
                }
            }
        });
        mixedWaste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mixedWaste.isChecked()) {
                    mixedWaste.setChecked(false);
                    mixedWaste.setCheckMarkDrawable(null);
                } else {
                    mixedWaste.setChecked(true);
                    mixedWaste.setCheckMarkDrawable(R.drawable.ic_check);
                }
            }
        });
        dangereous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dangereous.isChecked()) {
                    dangereous.setChecked(false);
                    dangereous.setCheckMarkDrawable(null);
                } else {
                    dangereous.setChecked(true);
                    dangereous.setCheckMarkDrawable(R.drawable.ic_check);
                }
            }
        });
        textile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textile.isChecked()) {
                    textile.setChecked(false);
                    textile.setCheckMarkDrawable(null);
                } else {
                    textile.setChecked(true);
                    textile.setCheckMarkDrawable(R.drawable.ic_check);
                }
            }
        });

        ArrayList<String> categories = getArguments().getStringArrayList("categories");

        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i) == WasteCategory.BULKY.toString()) {
                bulky.setChecked(true);
                bulky.setCheckMarkDrawable(R.drawable.ic_check);
            }
            if (categories.get(i) == WasteCategory.TEXTILE.toString()) {
                textile.setChecked(true);
                textile.setCheckMarkDrawable(R.drawable.ic_check);
            }
            if (categories.get(i) == WasteCategory.PLASTIC_METAL.toString()) {
                plasticMetal.setChecked(true);
                plasticMetal.setCheckMarkDrawable(R.drawable.ic_check);
            }
            if (categories.get(i) == WasteCategory.PAPER_CARDBOARD.toString()) {
                paperCardboard.setChecked(true);
                paperCardboard.setCheckMarkDrawable(R.drawable.ic_check);
            }
            if (categories.get(i) == WasteCategory.MIXED_COMUNAL.toString()) {
                mixedWaste.setChecked(true);
                mixedWaste.setCheckMarkDrawable(R.drawable.ic_check);
            }
            if (categories.get(i) == WasteCategory.DANGEROUS.toString()) {
                dangereous.setChecked(true);
                dangereous.setCheckMarkDrawable(R.drawable.ic_check);
            }
            if (categories.get(i) == WasteCategory.BIOWASTE.toString()) {
                bioWaste.setChecked(true);
                bioWaste.setCheckMarkDrawable(R.drawable.ic_check);
            }
        }

        applyChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<WasteCategory> newList = new ArrayList<>();
                if (bulky.isChecked()) {
                    Log.d("govna", String.valueOf(bulky.isChecked()));
                    newList.add(WasteCategory.BULKY);
                }
                if (plasticMetal.isChecked()) {
                    newList.add(WasteCategory.PLASTIC_METAL);
                }
                if (paperCardboard.isChecked()) {
                    newList.add(WasteCategory.PAPER_CARDBOARD);
                }
                if (bioWaste.isChecked()) {
                    newList.add(WasteCategory.BIOWASTE);
                }
                if (mixedWaste.isChecked()) {
                    newList.add(WasteCategory.MIXED_COMUNAL);
                }
                if (dangereous.isChecked()) {
                    newList.add(WasteCategory.DANGEROUS);
                }
                if (textile.isChecked()) {
                    newList.add(WasteCategory.TEXTILE);
                }
                repository.updateDisposalPlace(new DisposalPlace(getArguments().getLong("id"), getArguments().getString("address"), time.getText().toString(), newList));
            }
        });


    }

    @Override
    public void apiResultChanged(Result result) {

        switch (result.getResultType()) {
            case UPDATE_DISPOSAL_PLACE: {
                Toast.makeText(getContext(), "Uspješna promjena", Toast.LENGTH_LONG).show();
                break;
            }
        }

    }
}
