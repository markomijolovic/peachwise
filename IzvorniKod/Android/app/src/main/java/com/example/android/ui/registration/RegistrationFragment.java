package com.example.android.ui.registration;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.android.R;
import com.example.android.constants.Constants;
import com.example.android.models.Result;
import com.example.android.models.UserBuilder;
import com.example.android.repository.AppRepository;
import com.example.android.ui.BaseFragment;

import org.jetbrains.annotations.NotNull;

public class RegistrationFragment extends BaseFragment {

    private AppRepository repository;
    private EditText password;
    private EditText userName;
    private EditText email;
    private EditText address;
    private EditText name;
    private EditText lastname;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        repository = AppRepository.Companion.getInstance();
        View root =  inflater.inflate(R.layout.fragment_registration, container, false);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        repository.setUsernameAndPassword("admin", "admin");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        final Button btn_reg = getActivity().findViewById(R.id.btn_send);
        userName = getActivity().findViewById(R.id.box_username);
        password = getActivity().findViewById(R.id.box_pswd);
        email = getActivity().findViewById(R.id.box_email);
        address = getActivity().findViewById(R.id.box_address);
        name = getActivity().findViewById(R.id.box_ime);
        lastname = getActivity().findViewById(R.id.box_prezime);
        btn_reg.setOnClickListener(v -> registerUser());
    }

    private void registerUser() {
        UserBuilder userBuilder = new UserBuilder(userName.getText().toString(), password.getText().toString());

        userBuilder.setEmail(email.getText().toString());
        userBuilder.setAddress(address.getText().toString());
        userBuilder.setFirstName(name.getText().toString());
        userBuilder.setLastName(lastname.getText().toString());

        repository.registerUser(userBuilder.build());
    }

    private void userRegistered() {
        final String username = userName.getText().toString();
        final String pass = password.getText().toString();

        repository.setUsernameAndPassword(username, pass);
    }


    @SuppressWarnings("ConstantConditions")
    @Override
    public void apiResultChanged(@NotNull Result result) {
        switch (result.getResultType()) {
            case REGISTER_USER: {
                userRegistered();
                getActivity().getSupportFragmentManager().popBackStack();
            }
        }
    }

}
