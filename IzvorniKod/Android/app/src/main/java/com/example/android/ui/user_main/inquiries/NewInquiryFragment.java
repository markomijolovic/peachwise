package com.example.android.ui.user_main.inquiries;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.android.R;
import com.example.android.enums.ResourceType;
import com.example.android.enums.WasteCategory;
import com.example.android.models.Complaint;
import com.example.android.models.RemovalRequest;
import com.example.android.models.ResourceRequest;
import com.example.android.models.Result;
import com.example.android.repository.AppRepository;
import com.example.android.ui.BaseFragment;
import com.google.android.material.textfield.TextInputEditText;

public class NewInquiryFragment extends BaseFragment {

    String selectedInquiryType;
    WasteCategory selectedWasteCategory;
    ResourceType selectedResourceType;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_new_inquiry, container, false);

        final AppRepository repository = AppRepository.Companion.getInstance();
        repository.subscribeToResult(this);

        ConstraintLayout request_removal_layout = root.findViewById(R.id.request_removal_form);
        ConstraintLayout complaint_layout = root.findViewById(R.id.complaint_form);
        ConstraintLayout resource_request_layout = root.findViewById(R.id.resource_request_form);

        Button submitButton = root.findViewById(R.id.new_inquiry_submit);
        EditText requestRemovalVolume = root.findViewById(R.id.request_removal_volume);
        TextInputEditText complaintMessage = root.findViewById(R.id.complaint_message);
        EditText resourceRequestQuantity = root.findViewById(R.id.resource_request_quantity);

        Spinner spinner = (Spinner) root.findViewById(R.id.new_inquiry_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.inquiry_filter, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        selectedInquiryType = "complaints";
                        request_removal_layout.setVisibility(View.GONE);
                        resource_request_layout.setVisibility(View.GONE);
                        complaint_layout.setVisibility(View.VISIBLE);

                        break;
                    case 1:
                        selectedInquiryType = "request_removal";
                        Spinner CategorySpinner = (Spinner) root.findViewById(R.id.request_removal_spinner);
                        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                                R.array.waste_type_list, android.R.layout.simple_spinner_item);
                        if (CategorySpinner.getAdapter() != adapter) {
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            CategorySpinner.setAdapter(adapter);
                        }

                        CategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                switch (position) {
                                    case 0: {
                                        selectedWasteCategory = WasteCategory.BULKY;
                                        break;
                                    }
                                    case 1: {
                                        selectedWasteCategory = WasteCategory.PLASTIC_METAL;
                                        break;
                                    }
                                    case 2: {
                                        selectedWasteCategory = WasteCategory.PAPER_CARDBOARD;
                                        break;
                                    }
                                    case 3: {
                                        selectedWasteCategory = WasteCategory.BIOWASTE;
                                        break;
                                    }
                                    case 4: {
                                        selectedWasteCategory = WasteCategory.MIXED_COMUNAL;
                                        break;
                                    }
                                    case 5:{
                                        selectedWasteCategory = WasteCategory.TEXTILE;
                                        break;
                                    }
                                    case 6:{
                                        selectedWasteCategory = WasteCategory.DANGEROUS;
                                        break;
                                    }
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                                // sometimes you need nothing here
                            }
                        });
                        complaint_layout.setVisibility(View.GONE);
                        resource_request_layout.setVisibility(View.GONE);
                        request_removal_layout.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        selectedInquiryType = "resource_request";
                        Spinner CategorySpinnerResource = (Spinner) root.findViewById(R.id.resource_request_spinner);
                        ArrayAdapter<CharSequence> adapterResource = ArrayAdapter.createFromResource(getActivity(),
                                R.array.waste_type_request_list, android.R.layout.simple_spinner_item);
                        if (CategorySpinnerResource.getAdapter() != adapterResource) {
                            adapterResource.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            CategorySpinnerResource.setAdapter(adapterResource);
                        }

                        CategorySpinnerResource.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                switch (position) {
                                    case 0:
                                        selectedWasteCategory = WasteCategory.PLASTIC_METAL;
                                        break;
                                    case 1:
                                        selectedWasteCategory = WasteCategory.PAPER_CARDBOARD;
                                        break;
                                    case 2:
                                        selectedWasteCategory = WasteCategory.BIOWASTE;
                                        break;
                                    case 3:
                                        selectedWasteCategory = WasteCategory.MIXED_COMUNAL;
                                        break;
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                                // sometimes you need nothing here
                            }
                        });
                        Spinner TypeSpinnerResource = (Spinner) root.findViewById(R.id.resource_request_wc_spinner);
                        ArrayAdapter<CharSequence> adapterResourceType = ArrayAdapter.createFromResource(getActivity(),
                                R.array.resource_type_list, android.R.layout.simple_spinner_item);
                        if (TypeSpinnerResource.getAdapter() != adapterResourceType) {
                            adapterResourceType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            TypeSpinnerResource.setAdapter(adapterResourceType);
                        }

                        TypeSpinnerResource.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                switch (position) {
                                    case 0:
                                        selectedResourceType = ResourceType.BAG;
                                        break;
                                    case 1:
                                        selectedResourceType = ResourceType.BUCKET;
                                        break;
                                    case 2:
                                        selectedResourceType = ResourceType.CONTAINER;
                                        break;
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                                // sometimes you need nothing here
                            }
                        });
                        complaint_layout.setVisibility(View.GONE);
                        request_removal_layout.setVisibility(View.GONE);
                        resource_request_layout.setVisibility(View.VISIBLE);
                        break;
                }


                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (selectedInquiryType.equals("complaints")) {

                            repository.postComplaint(new Complaint(null, null, null
                                    , complaintMessage.getText().toString(), null, null));

                        } else if (selectedInquiryType.equals("request_removal")) {
                            repository.postRemovalRequest(new RemovalRequest(null,
                                    null, parseDouble(requestRemovalVolume.getText().toString()),
                                    selectedWasteCategory, null, null, null));

                        } else if (selectedInquiryType.equals("resource_request")) {
                            repository.postResourceRequest(new ResourceRequest(null,
                                    null, parseInt(resourceRequestQuantity.getText().toString()),
                                    selectedWasteCategory, selectedResourceType, null, null,
                                    null));
                        }

                    }
                });
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                // sometimes you need nothing here
            }
        });

        return root;
    }

    private Double parseDouble(String number) {
        try {
            return Double.parseDouble(number);
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    private Integer parseInt(String number) {
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    @Override
    public void apiResultChanged(Result result) {
        switch (result.getResultType()) {
            case POST_INQUIRY: {
                Toast.makeText(getContext(), "Uspješno poslano", Toast.LENGTH_SHORT).show();
                break;
            }
        }

    }
}
