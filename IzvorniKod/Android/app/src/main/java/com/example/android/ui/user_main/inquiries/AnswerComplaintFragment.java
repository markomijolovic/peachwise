package com.example.android.ui.user_main.inquiries;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.android.R;
import com.example.android.enums.ResultType;
import com.example.android.models.Complaint;
import com.example.android.models.Inquiry;
import com.example.android.models.Result;
import com.example.android.repository.AppRepository;
import com.example.android.ui.BaseFragment;
import com.google.gson.Gson;

//TODO: Jaksa -> napraviti layout i logiku, zaposlenik ovdje moze samo mijenjati vrijeme odvoza, pa adekvatno tome napraviti sve
public class AnswerComplaintFragment extends BaseFragment {

    private TextView complaintText;
    private Button submitButton;
    private EditText answerText;
    private Switch isProcessed;
    private Switch isApproved;

    private AppRepository appRepository = AppRepository.Companion.getInstance();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root =  inflater.inflate(R.layout.fragment_answer_complaint, container, false);
        complaintText = root.findViewById(R.id.tv_complaint_text);
        submitButton = root.findViewById(R.id.btn_submit);
        answerText = root.findViewById(R.id.et_answer_text);
        isProcessed = root.findViewById(R.id.sw_processed);
        isApproved = root.findViewById(R.id.sw_approved);
        return root;
    }

    //ovdje je najbolje inicijalizirat viowe, ne treba ti onaj root. sranje nego getActivity()
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String json = getArguments().getString("extras");
        Inquiry complaint = new Gson().fromJson(json, Inquiry.class);
        complaintText.setText(complaint.getRequestText());

        submitButton.setOnClickListener(v -> submitData(complaint));

    }

    private void submitData(Inquiry complaint) {
        boolean approved = isApproved.isChecked();
        boolean processed = isProcessed.isChecked();
        String answer = answerText.getText().toString();
        String question = complaintText.getText().toString();

        Complaint complaint1 = Complaint.Companion.createResponse(answer, approved, processed);
        appRepository.updateComplaint(complaint.getId(), complaint1);
    }

    @Override
    public void apiResultChanged(Result result) {
        switch (result.getResultType()) {
            case UPDATE_INQUIRY: {
                Toast.makeText(getActivity(), "Success!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
