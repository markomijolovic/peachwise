package com.example.android.utils
import com.example.android.models.User

object UserStorage {

    private var user: User? = null

    fun storeCurrentUser(user: User) {
        this.user = user
    }

    fun getCurrentUser() = user
}