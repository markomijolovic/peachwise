package com.example.android.ui;

import android.view.View;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;

import com.example.android.interfaces.ApiErrorListener;
import com.example.android.interfaces.ApiResultListener;
import com.example.android.models.Result;
import com.example.android.repository.AppRepository;

public abstract class BaseFragment extends Fragment implements ApiResultListener {

    final private AppRepository repo = AppRepository.Companion.getInstance();
    protected ProgressBar progressBar;

    @Override
    public void onResume() {
        super.onResume();
        repo.subscribeToResult(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        repo.unsubscribeFromResult(this);
    }

    @Override
    public void apiResultRequested() {

    }
}
