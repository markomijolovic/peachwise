package com.SpringBoot.rest;

import com.SpringBoot.entity.Person;
import com.SpringBoot.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class PersonController
{
    @Autowired
    private PersonService personService;

    @GetMapping("")
    @Secured("ROLE_ADMIN")
    public List<Person> listPersons()
    {
        return personService.listAll();
    }

    @GetMapping("/{username}")
    @Secured({"ROLE_ADMIN", "ROLE_EMPLOYEE", "ROLE_USER"})
    public Person getPerson(@PathVariable("username") String username, @AuthenticationPrincipal UserDetails userDetails)
    {
        return personService.fetch(username, userDetails);
    }

    @PutMapping("/{username}")
    @Secured({"ROLE_ADMIN", "ROLE_EMPLOYEE", "ROLE_USER"})
    public Person updatePerson(@PathVariable("username") String username, @RequestBody Person person, @AuthenticationPrincipal UserDetails userDetails)
    {
        return personService.updatePerson(username, person, userDetails);
    }

    @DeleteMapping("/{username}")
    @Secured({"ROLE_ADMIN", "ROLE_EMPLOYEE", "ROLE_USER"})
    public Person deletePerson(@PathVariable("username") String username, @AuthenticationPrincipal UserDetails userDetails)
    {
        return personService.deletePerson(username, userDetails);
    }
}
