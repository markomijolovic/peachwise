package com.SpringBoot.rest;

import com.SpringBoot.entity.Removal;
import com.SpringBoot.service.RemovalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/removals")
public class RemovalController
{
    @Autowired
    private RemovalService removalService;

    @GetMapping("/{cityBlock}")
    @Secured({"ROLE_ADMIN", "ROLE_EMPLOYEE", "ROLE_USER"})
    public List<Removal> getRemoval(@PathVariable("cityBlock") String cityBlock, @AuthenticationPrincipal UserDetails userDetails)
    {
        return removalService.findAllByCityBlock(cityBlock);
    }

    @PutMapping("/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_EMPLOYEE"})
    public Removal updateRemoval(@PathVariable("id") Long id, @RequestBody Removal removal, @AuthenticationPrincipal UserDetails userDetails)
    {
        return removalService.updateRemoval(id, removal);
    }

    @GetMapping("")
    @Secured({"ROLE_ADMIN", "ROLE_EMPLOYEE", "ROLE_USER"})
    public List<Removal> listAll()
    {
        return removalService.listAll();
    }
}