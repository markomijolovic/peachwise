package com.SpringBoot.rest;

import com.SpringBoot.entity.Inquiry;
import com.SpringBoot.service.InquiryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/inquiries")
public class InquiryController
{
    @Autowired
    private InquiryService inquiryService;

    @GetMapping("")
    @Secured({"ROLE_EMPLOYEE", "ROLE_USER"})
    public List<Inquiry> listInquiries(@AuthenticationPrincipal UserDetails userDetails)
    {
        return inquiryService.listAll(userDetails);
    }

    @PostMapping("")
    @Secured({"ROLE_USER"})
    public Inquiry createInquiry(@RequestBody Inquiry inquiry, @AuthenticationPrincipal UserDetails userDetails)
    {
        return inquiryService.createInquiry(inquiry, userDetails);
    }

    @PutMapping("/{id}")
    @Secured({"ROLE_EMPLOYEE"})
    public Inquiry updateInquiry(@PathVariable("id") Long id, @RequestBody Inquiry inquiry, @AuthenticationPrincipal UserDetails userDetails)
    {
        return inquiryService.updateInquiry(id, inquiry, userDetails);
    }
}
