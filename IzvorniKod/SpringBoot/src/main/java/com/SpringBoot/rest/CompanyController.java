package com.SpringBoot.rest;

import com.SpringBoot.entity.Company;
import com.SpringBoot.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CompanyController
{
    @Autowired
    private CompanyService companyService;

    @GetMapping("/company")
    @Secured({"ROLE_ADMIN", "ROLE_EMPLOYEE", "ROLE_USER"})
    public Company getCompany()
    {
        return companyService.getCompany();
    }
}
