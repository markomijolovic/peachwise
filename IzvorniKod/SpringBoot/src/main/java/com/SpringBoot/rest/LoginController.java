package com.SpringBoot.rest;

import com.SpringBoot.entity.Person;
import com.SpringBoot.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController
{
    @Autowired
    private PersonService personService;

    @PostMapping("/login")
    @Secured({"ROLE_ADMIN", "ROLE_EMPLOYEE", "ROLE_USER"})
    public Person login(@AuthenticationPrincipal UserDetails userDetails)
    {
        return personService.findByUsername(userDetails.getUsername());
    }

    @PostMapping("/register")
    public Person createPerson(@RequestBody Person person)
    {
        return personService.createPerson(person);
    }
}
