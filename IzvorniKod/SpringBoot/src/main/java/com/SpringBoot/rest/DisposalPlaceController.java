package com.SpringBoot.rest;

import com.SpringBoot.entity.DisposalPlace;
import com.SpringBoot.service.DisposalPlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/disposal_places")
public class DisposalPlaceController
{
    @Autowired
    private DisposalPlaceService disposalPlaceService;

    @GetMapping("")
    @Secured({"ROLE_ADMIN", "ROLE_EMPLOYEE", "ROLE_USER"})
    public List<DisposalPlace> listAll(@AuthenticationPrincipal UserDetails userDetails)
    {
        return disposalPlaceService.listAll();
    }

    @PutMapping("/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_EMPLOYEE"})
    public DisposalPlace updateDisposalPlace(@PathVariable("id") Long id, @RequestBody DisposalPlace disposalPlace, @AuthenticationPrincipal UserDetails userDetails)
    {
        return disposalPlaceService.updateDisposalPlace(id, disposalPlace, userDetails);
    }
}
