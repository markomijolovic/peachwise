package com.SpringBoot.dao;

import com.SpringBoot.entity.Removal;
import com.SpringBoot.entity.enums.WasteCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RemovalRepository extends JpaRepository<Removal, Long>
{
    List<Removal> findAllByCityBlock(String cityBlock);

    Optional<Removal> findById(Long terminId);

    boolean existsByCityBlockAndWasteCategory(String cityBlock, WasteCategory wasteCategory);
}