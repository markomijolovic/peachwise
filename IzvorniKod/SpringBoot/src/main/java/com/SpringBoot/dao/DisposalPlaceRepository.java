package com.SpringBoot.dao;

import com.SpringBoot.entity.DisposalPlace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DisposalPlaceRepository extends JpaRepository<DisposalPlace, Long>
{
    boolean existsByAddress(String address);
}
