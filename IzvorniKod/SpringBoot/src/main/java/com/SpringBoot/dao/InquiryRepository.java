package com.SpringBoot.dao;

import com.SpringBoot.entity.Inquiry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InquiryRepository extends JpaRepository<Inquiry, Long>
{
    List<Inquiry> findAllByIsProcessedFalse();

    List<Inquiry> findByPersonId(Long id);
}
