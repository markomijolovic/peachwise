package com.SpringBoot.entity;

import com.SpringBoot.entity.enums.WasteCategory;

import javax.persistence.*;

@Entity
public class Removal
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String cityBlock;
    private String time;
    @Enumerated(EnumType.STRING)
    private WasteCategory wasteCategory;

    public String getCityBlock()
    {
        return cityBlock;
    }

    public void setCityBlock(String cityBlock)
    {
        this.cityBlock = cityBlock;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    public WasteCategory getWasteCategory()
    {
        return wasteCategory;
    }

    public void setWasteCategory(WasteCategory wasteCategory)
    {
        this.wasteCategory = wasteCategory;
    }

    @Override
    public String toString()
    {
        return String.format("Termin #%d u cetvrti : %s u %s", id, cityBlock, time);
    }

}
