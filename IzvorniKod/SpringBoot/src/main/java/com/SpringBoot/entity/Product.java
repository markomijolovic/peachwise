package com.SpringBoot.entity;

import com.SpringBoot.entity.enums.WasteCategory;

import javax.persistence.*;

@Entity
public class Product
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private WasteCategory wasteCategory;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String nameOfProduct)
    {
        this.name = nameOfProduct;
    }

    public WasteCategory getWasteCategory()
    {
        return wasteCategory;
    }

    public void setWasteCategory(WasteCategory wasteCategory)
    {
        this.wasteCategory = wasteCategory;
    }

    public String toString()
    {
        return String.format("Waste #%d %s %s role: %s", id, name, wasteCategory);
    }

}
