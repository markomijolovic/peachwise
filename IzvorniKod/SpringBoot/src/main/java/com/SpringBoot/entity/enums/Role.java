package com.SpringBoot.entity.enums;

public enum Role
{
    ROLE_USER, ROLE_ADMIN, ROLE_EMPLOYEE
}
