package com.SpringBoot.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

public class CustomPersonDetails implements UserDetails
{

    private static final long serialVersionUID = 1L;
    private Person person;

    public CustomPersonDetails(Person person)
    {
        this.person = person;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        var ret = new ArrayList<SimpleGrantedAuthority>();
        ret.add(new SimpleGrantedAuthority(person.getRole()));
        return ret;
    }

    @Override
    public String getPassword()
    {
        return person.getPassword();
    }

    @Override
    public String getUsername()
    {
        return person.getUsername();
    }

    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }

    @Override
    public boolean isAccountNonLocked()
    {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    @Override
    public boolean isEnabled()
    {
        return true;
    }

    public Person getPerson()
    {
        return person;
    }
}
