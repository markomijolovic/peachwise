package com.SpringBoot.entity.enums;

public enum InquiryType
{
    COMPLAINT, REMOVAL_REQUEST, RESOURCE_REQUEST
}
