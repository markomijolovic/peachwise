package com.SpringBoot.entity;

import com.SpringBoot.entity.enums.WasteCategory;
import com.fasterxml.jackson.annotation.JsonBackReference;
import org.springframework.util.Assert;

import javax.persistence.*;

@Entity
public class RequestItem
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Double volume;
    @Enumerated(EnumType.STRING)
    private WasteCategory wasteCategory;
    @ManyToOne
    @JsonBackReference
    private Inquiry inquiry;

    public Inquiry getInquiry()
    {
        return inquiry;
    }

    public void setInquiry(Inquiry inquiry)
    {
        this.inquiry = inquiry;
    }

    public Double getVolume()
    {
        return volume;
    }

    public void setVolume(Double volume)
    {
        this.volume = volume;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public WasteCategory getWasteCategory()
    {
        return wasteCategory;
    }

    public void setWasteCategory(WasteCategory wasteCategory)
    {
        this.wasteCategory = wasteCategory;
    }

    @Override
    public String toString()
    {
        return String.format("Stavka zahtjeva #%d volumena %d ", id, volume);
    }

    public void validate()
    {
        Assert.notNull(volume, "Request item volume must not be null");
        Assert.notNull(wasteCategory, "Request item waste category must not be null");
    }
}
