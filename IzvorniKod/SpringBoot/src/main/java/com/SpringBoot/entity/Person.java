package com.SpringBoot.entity;

import com.SpringBoot.entity.enums.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class Person
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String username;
    private String password;
    private String address;
    private String email;
    private String firstName;
    private String lastName;
    @Enumerated(EnumType.STRING)
    private Role role;
    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    @JsonManagedReference
    @JsonIgnore
    private List<Inquiry> inquiries;

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getRole()
    {
        return role.name();
    }

    public void setRole(Role role)
    {
        this.role = role;
    }

    @JsonIgnore
    public List<Inquiry> getInquiries()
    {
        return inquiries;
    }

    @JsonIgnore
    public void setInquiries(List<Inquiry> inquiries)
    {
        this.inquiries = inquiries;
    }

    @Override
    public String toString()
    {
        return String.format("Person #%d %s %s role: %s", id, firstName, lastName, role);
    }
}
