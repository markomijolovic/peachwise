package com.SpringBoot.entity;

import com.SpringBoot.entity.enums.InquiryType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class Inquiry
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Boolean isProcessed;
    private String requestText;
    @CreationTimestamp
    private Timestamp timestamp;
    @ManyToOne
    @JsonBackReference
    private Person person;
    @Enumerated(EnumType.STRING)
    private InquiryType inquiryType;
    @OneToMany(mappedBy = "inquiry", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Resource> resources;
    @OneToMany(mappedBy = "inquiry", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<RequestItem> requestItems;

    // TODO: add this to documentation
    private Boolean isApproved;
    // TODO: add this to documentation
    private String responseText;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }


    public Boolean isProcessed()
    {
        return isProcessed;
    }

    public void setProcessed(Boolean isProcessed)
    {
        this.isProcessed = isProcessed;
    }

    public String getRequestText()
    {
        return requestText;
    }

    public void setRequestText(String requestText)
    {
        this.requestText = requestText;
    }

    public Person getPerson()
    {
        return person;
    }

    public void setPerson(Person person)
    {
        this.person = person;
    }

    public InquiryType getInquiryType()
    {
        return inquiryType;
    }

    public void setInquiryType(InquiryType inquiryType)
    {
        this.inquiryType = inquiryType;
    }

    public List<RequestItem> getRequestItems()
    {
        return requestItems;
    }

    public void setRequestItems(List<RequestItem> requestItems)
    {
        this.requestItems = requestItems;
    }

    public List<Resource> getResources()
    {
        return resources;
    }

    public void setResources(List<Resource> resources)
    {
        this.resources = resources;
    }

    public Boolean isApproved()
    {
        return isApproved;
    }

    public void setApproved(Boolean approved)
    {
        isApproved = approved;
    }

    public String getResponseText()
    {
        return responseText;
    }

    public void setResponseText(String responseText)
    {
        this.responseText = responseText;
    }

    public Timestamp getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp)
    {
        this.timestamp = timestamp;
    }
}
