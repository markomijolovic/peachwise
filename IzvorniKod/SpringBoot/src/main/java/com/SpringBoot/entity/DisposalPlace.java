package com.SpringBoot.entity;

import com.SpringBoot.entity.enums.WasteCategory;

import javax.persistence.*;
import java.util.List;

@Entity
public class DisposalPlace
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String address;
    private String workingHours;
    @ElementCollection
    @Enumerated(EnumType.STRING)
    // TODO: change this
    @OrderColumn
    private List<WasteCategory> wasteCategories;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getWorkingHours()
    {
        return workingHours;
    }

    public void setWorkingHours(String workingHours)
    {
        this.workingHours = workingHours;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public List<WasteCategory> getWasteCategories()
    {
        return wasteCategories;
    }

    public void setWasteCategories(List<WasteCategory> wasteCategories)
    {
        this.wasteCategories = wasteCategories;
    }


}
