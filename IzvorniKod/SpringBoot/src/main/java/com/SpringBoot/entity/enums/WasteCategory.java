package com.SpringBoot.entity.enums;

public enum WasteCategory
{
    MIXED_COMUNAL, BIOWASTE, PAPER_CARDBOARD, PLASTIC_METAL, BULKY, TEXTILE, DANGEROUS
}
