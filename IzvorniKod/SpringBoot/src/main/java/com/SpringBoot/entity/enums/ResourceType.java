package com.SpringBoot.entity.enums;

public enum ResourceType
{
    BUCKET, BAG, CONTAINER
}
