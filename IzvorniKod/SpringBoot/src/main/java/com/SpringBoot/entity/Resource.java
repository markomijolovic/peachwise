package com.SpringBoot.entity;

import com.SpringBoot.entity.enums.ResourceType;
import com.SpringBoot.entity.enums.WasteCategory;
import com.fasterxml.jackson.annotation.JsonBackReference;
import org.springframework.util.Assert;

import javax.persistence.*;

@Entity
public class Resource
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Integer quantity;
    @Enumerated(EnumType.STRING)
    private WasteCategory wasteCategory;
    @Enumerated(EnumType.STRING)
    private ResourceType resourceType;
    @ManyToOne
    @JsonBackReference
    private Inquiry inquiry;

    public Inquiry getInquiry()
    {
        return inquiry;
    }

    public void setInquiry(Inquiry inquiry)
    {
        this.inquiry = inquiry;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Integer getQuantity()
    {
        return quantity;
    }

    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }

    public WasteCategory getWasteCategory()
    {
        return wasteCategory;
    }

    public void setWasteCategory(WasteCategory wasteCategory)
    {
        this.wasteCategory = wasteCategory;
    }

    public ResourceType getResourceType()
    {
        return resourceType;
    }

    public void setResourceType(ResourceType resource)
    {
        this.resourceType = resource;
    }

    public void validate()
    {
        Assert.notNull(quantity, "Resource quantity must not be null");
        Assert.notNull(wasteCategory, "Resource category must not be null");
        Assert.notNull(resourceType, "Resource type must not be null");
    }


    @Override
    public String toString()
    {
        return String.format("Resource #%d %d %s %s %d", id, quantity, wasteCategory, resourceType, inquiry);
    }
}
