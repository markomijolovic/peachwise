package com.SpringBoot.service.serviceImpl;


import com.SpringBoot.dao.ProductRepository;
import com.SpringBoot.entity.Product;
import com.SpringBoot.service.EntityMissingException;
import com.SpringBoot.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class ProductServiceJpa implements ProductService
{
    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> listAll()
    {
        return productRepository.findAll();
    }

    @Override
    public Product updateProduct(Long id, Product new_product)
    {
        Assert.notNull(new_product, "Proizvod mora biti zadan.");
        Assert.notNull(new_product.getWasteCategory(), "Kategorija otpada kojoj proizvod pripada mora biti zadana.");

        var product = productRepository.findById(id).orElseThrow(() -> new EntityMissingException(String.format("Proizvod s ID-em %d ne postoji.", id)));
        product.setWasteCategory(new_product.getWasteCategory());

        return productRepository.save(product);
    }

    @Override
    public Product fetch(String productName)
    {
        return productRepository.findByName(productName).orElseThrow(() -> new EntityMissingException(String.format("Proizvod imena %s ne postoji.", productName)));
    }
}
