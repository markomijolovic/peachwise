package com.SpringBoot.service.serviceImpl;

import com.SpringBoot.dao.CompanyRepository;
import com.SpringBoot.entity.Company;
import com.SpringBoot.service.CompanyService;
import com.SpringBoot.service.EntityMissingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyServiceJpa implements CompanyService
{
    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public Company getCompany()
    {
        var company = companyRepository.findAll();
        if (company.isEmpty())
        {
            throw new EntityMissingException("Informacije o poduzeću ne postoje.");
        }
        return company.get(0);
    }
}
