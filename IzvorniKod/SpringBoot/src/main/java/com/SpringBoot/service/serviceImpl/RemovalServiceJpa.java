package com.SpringBoot.service.serviceImpl;

import com.SpringBoot.dao.RemovalRepository;
import com.SpringBoot.entity.Removal;
import com.SpringBoot.service.EntityMissingException;
import com.SpringBoot.service.RemovalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class RemovalServiceJpa implements RemovalService
{
    @Autowired
    RemovalRepository removalRepository;

    @Override
    public List<Removal> findAllByCityBlock(String cityBlock)
    {
        Assert.hasText(cityBlock, "Ime gradske četvrti ne smije biti prazno.");
        return removalRepository.findAllByCityBlock(cityBlock);
    }

    @Override
    public Removal updateRemoval(Long id, Removal new_removal)
    {
        Assert.notNull(new_removal, "Termin odvoza mora biti zadan.");

        var removal = removalRepository.findById(id).orElseThrow(() -> new EntityMissingException(String.format("Termin odvoza s ID-em %s ne postoji.", id)));

        //        if (new_removal.getCityBlock() != null)
        //        {
        //            Assert.hasText(new_removal.getCityBlock(), "City block must not be blank");
        //            removal.setCityBlock(new_removal.getCityBlock());
        //        }

        if (new_removal.getTime() != null)
        {
            Assert.hasText(new_removal.getTime(), "Vrijeme termina odvoza ne smije biti prazno.");
            removal.setTime(new_removal.getTime());
        }

        //        if (new_removal.getWasteCategory() != null)
        //        {
        //            removal.setWasteCategory(new_removal.getWasteCategory());
        //        }

        return removalRepository.save(removal);
    }

    @Override
    public List<Removal> listAll()
    {
        return removalRepository.findAll();
    }
}
