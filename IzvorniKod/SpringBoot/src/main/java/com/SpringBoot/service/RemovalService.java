package com.SpringBoot.service;

import com.SpringBoot.entity.Removal;

import java.util.List;

public interface RemovalService
{
    List<Removal> findAllByCityBlock(String cityBlock);

    Removal updateRemoval(Long id, Removal removal);

    List<Removal> listAll();
}
