package com.SpringBoot.service.serviceImpl;

import com.SpringBoot.dao.InquiryRepository;
import com.SpringBoot.dao.PersonRepository;
import com.SpringBoot.entity.Inquiry;
import com.SpringBoot.service.InquiryService;
import com.SpringBoot.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class InquiryServiceJpa implements InquiryService
{
    @Autowired
    InquiryRepository inquiryRepository;

    @Autowired
    PersonRepository personRepository;

    @Override
    public List<Inquiry> listAll(UserDetails userDetails)
    {
        var authorities = userDetails.getAuthorities();
        if (authorities.stream().map(GrantedAuthority::getAuthority).anyMatch(role_string -> role_string.equals("ROLE_USER")))
        {
            // return only that user's inquiries
            // person object exists because it is used for authentication
            var person = personRepository.findByUsername(userDetails.getUsername()).get();
            return inquiryRepository.findByPersonId(person.getId());
        }
        else
        {
            // EMPLOYEE -> return only inquiries that haven't been procdessed
            return inquiryRepository.findAllByIsProcessedFalse();
        }
    }

    @Override
    public Inquiry createInquiry(Inquiry inquiry, UserDetails userDetails)
    {
        Assert.isTrue(inquiry.getInquiryType() != null, "Tip zahtjeva mora biti zadan.");

        switch (inquiry.getInquiryType())
        {
            case COMPLAINT:
                Assert.isTrue(inquiry.getRequestText() != null && !inquiry.getRequestText().isBlank(), "Pritužba mora sadržavati tekstualni opis.");
                inquiry.setResources(null);
                inquiry.setRequestItems(null);
                break;
            case REMOVAL_REQUEST:
                Assert.isTrue(inquiry.getRequestItems() != null && !inquiry.getRequestItems().isEmpty(), "Zahtjev za odvozom mora sadržavati stavke zahtjeva.");
                inquiry.setResources(null);
                inquiry.setRequestText(null);
                break;
            case RESOURCE_REQUEST:
                Assert.isTrue(inquiry.getResources() != null && !inquiry.getResources().isEmpty(), "Zahtjev za dodatnim resursima mora sadržavati resurse.");
                inquiry.setRequestItems(null);
                inquiry.setRequestText(null);
                break;
        }

        inquiry.setApproved(null);
        inquiry.setProcessed(false);
        inquiry.setResponseText(null);

        // this will not fail
        var person = personRepository.findByUsername(userDetails.getUsername()).get();
        inquiry.setPerson(person);
        if (inquiry.getResources() != null)
        {
            for (var resource : inquiry.getResources())
            {
                resource.validate();
                resource.setInquiry(inquiry);
            }
        }

        if (inquiry.getRequestItems() != null)
        {
            for (var requestItem : inquiry.getRequestItems())
            {
                requestItem.validate();
                requestItem.setInquiry(inquiry);
            }
        }

        return inquiryRepository.save(inquiry);
    }


    @Override
    public Inquiry updateInquiry(Long id, Inquiry new_inquiry, UserDetails userDetails)
    {
        Assert.notNull(new_inquiry, "Upit mora biti zadan.");

        var inquiry = inquiryRepository.findById(id).orElseThrow(() -> new RequestDeniedException(String.format("Upit s ID-om %d ne postoji.", id)));

        if (inquiry.isProcessed() != null && inquiry.isProcessed())
        {
            throw new RequestDeniedException(String.format("Upit s ID-om %d je već procesiran.", id));
        }

        if (new_inquiry.isProcessed() != null)
            inquiry.setProcessed(new_inquiry.isProcessed());
        if (new_inquiry.getResponseText() != null)
            inquiry.setResponseText(new_inquiry.getResponseText());
        if (new_inquiry.isApproved() != null)
            inquiry.setApproved(new_inquiry.isApproved());
        return inquiryRepository.save(inquiry);
    }
}
