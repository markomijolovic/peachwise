package com.SpringBoot.service;

import com.SpringBoot.entity.Inquiry;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface InquiryService
{
    List<Inquiry> listAll(UserDetails userDetails);

    Inquiry createInquiry(Inquiry inquiry, UserDetails userDetails);

    Inquiry updateInquiry(Long id, Inquiry inquiry, UserDetails user);
}
