package com.SpringBoot.service;

import com.SpringBoot.entity.Company;

public interface CompanyService
{
    Company getCompany();
}
