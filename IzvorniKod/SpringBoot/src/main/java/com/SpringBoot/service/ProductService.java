package com.SpringBoot.service;

import com.SpringBoot.entity.Product;

import java.util.List;


public interface ProductService
{
    Product fetch(String productName);

    Product updateProduct(Long id, Product product);

    List<Product> listAll();

}
