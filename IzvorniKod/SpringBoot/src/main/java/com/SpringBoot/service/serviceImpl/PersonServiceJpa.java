package com.SpringBoot.service.serviceImpl;

import com.SpringBoot.dao.PersonRepository;
import com.SpringBoot.entity.Person;
import com.SpringBoot.entity.enums.Role;
import com.SpringBoot.service.EntityMissingException;
import com.SpringBoot.service.PersonService;
import com.SpringBoot.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class PersonServiceJpa implements PersonService
{
    static final String PASSWORD_FORMAT = "[(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\\\S+$)]{5,20}";

    static final String EMAIL_FORMAT = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<Person> listAll()
    {
        return personRepository.findAll();
    }

    @Override
    public Person findByUsername(String username)
    {
        return personRepository.findByUsername(username).orElseThrow(() -> new EntityMissingException(String.format("Korisnik s korisničkim imenom %s ne postoji.", username)));
    }

    @Override
    public Person fetch(String username, UserDetails user)
    {
        Assert.isTrue(user.getUsername().equals(username), "Korisnik može dohvatiti samo svoj profil.");
        return personRepository.findByUsername(username).orElseThrow(() -> new EntityMissingException(String.format("Korisnik s korisničkim imenom %s ne postoji.", username)));
    }

    @Override
    public Person createPerson(Person person)
    {
        Assert.notNull(person, "Korisnik mora biti zadan.");
        Assert.hasText(person.getUsername(), "Korisničko ime ne smije biti prazno.");
        if (personRepository.existsByUsername(person.getUsername()))
        {
            throw new RequestDeniedException(String.format("Korisnik s korisničkim imenom %s već postoji.", person.getUsername()));
        }

        var password = person.getPassword();
        Assert.notNull(password, "Lozinka mora biti zadana.");
        Assert.isTrue(password.matches(PASSWORD_FORMAT), String.format("Lozinka mora sadržavati između 5 i 20 znakova.", password));

        var email = person.getEmail();
        Assert.notNull(email, "Adresa elektroničke pošte mora biti zadana.");
        Assert.isTrue(email.matches(EMAIL_FORMAT), "Adresa elektroničke pošte nije valjana.");
        if (personRepository.existsByEmail(email))
            throw new RequestDeniedException(String.format("Korisnik s adresom elektroničke pošte %s već postoji.", person.getEmail()));

        Assert.hasText(person.getFirstName(), "Ime ne smije biti prazno.");
        Assert.hasText(person.getLastName(), "Prezime ne smije biti prazno.");
        Assert.hasText(person.getAddress(), "Adresa ne smije biti prazna.");

        person.setRole(Role.ROLE_USER);  // only allow creation of unprivileged users
        person.setPassword(passwordEncoder.encode(person.getPassword()));
        return personRepository.save(person);
    }

    @Override
    public Person updatePerson(String username, Person new_person, UserDetails user)
    {
        Assert.isTrue(user.getUsername().equals(username), "Korisnik može ažurirati samo svoj profil.");
        Assert.notNull(new_person, "Korisnik mora biti zadan.");

        // this will not fail
        var person = personRepository.findByUsername(username).get();

        if (new_person.getUsername() != null)
        {
            Assert.isTrue(new_person.getUsername().equals(person.getUsername()) || !personRepository.existsByUsername(new_person.getUsername()), String.format("Korisnik s korisničkim imenom %s već postoji.", new_person.getUsername()));
            person.setUsername(new_person.getUsername());
        }
        if (new_person.getPassword() != null)
        {
            Assert.isTrue(new_person.getPassword().matches(PASSWORD_FORMAT), String.format("Lozinka mora sadržavati između 5 i 20 znakova. '%s'", new_person.getPassword()));
            person.setPassword(passwordEncoder.encode(new_person.getPassword()));
        }
        if (new_person.getAddress() != null)
        {
            Assert.hasText(new_person.getAddress(), "Adresa ne smije biti prazna.");
            person.setAddress(new_person.getAddress());
        }
        if (new_person.getEmail() != null)
        {
            Assert.isTrue(new_person.getEmail().equals(person.getEmail()) || !personRepository.existsByEmail(new_person.getEmail()), String.format("Person with email %s already exists", new_person.getEmail()));
            Assert.isTrue(new_person.getEmail().matches(EMAIL_FORMAT), "Adresa elektroničke pošte nije valjana.");

            person.setEmail(new_person.getEmail());
        }
        if (new_person.getFirstName() != null)
        {
            Assert.hasText(new_person.getFirstName(), "Ime ne smije biti prazno.");
            person.setFirstName(new_person.getFirstName());
        }
        if (new_person.getLastName() != null)
        {
            Assert.hasText(new_person.getFirstName(), "Prezime ne smije biti prazno.");
            person.setLastName(new_person.getLastName());
        }

        return personRepository.save(person);
    }

    @Override
    public Person deletePerson(String username, UserDetails user)
    {
        var authorities = user.getAuthorities();
        Assert.isTrue(authorities.stream().map(GrantedAuthority::getAuthority).anyMatch(role_string -> role_string.equals("ROLE_ADMIN")) || user.getUsername().equals(username), "Korisnik može obrisati samo svoj profil.");
        var person = personRepository.findByUsername(username).orElseThrow(() -> new EntityMissingException(String.format("Korisnik s korisničkim imenom %s ne postoji.", username)));
        personRepository.delete(person);
        return person;
    }
}
