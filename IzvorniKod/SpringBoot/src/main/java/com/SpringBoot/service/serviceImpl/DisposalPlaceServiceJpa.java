package com.SpringBoot.service.serviceImpl;

import com.SpringBoot.dao.DisposalPlaceRepository;
import com.SpringBoot.dao.PersonRepository;
import com.SpringBoot.entity.DisposalPlace;
import com.SpringBoot.service.DisposalPlaceService;
import com.SpringBoot.service.EntityMissingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class DisposalPlaceServiceJpa implements DisposalPlaceService
{
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private DisposalPlaceRepository disposalPlaceRepository;

    @Override
    public List<DisposalPlace> listAll()
    {
        return disposalPlaceRepository.findAll();
    }


    @Override
    public DisposalPlace updateDisposalPlace(Long id, DisposalPlace new_disposalPlace, UserDetails user)
    {
        Assert.notNull(new_disposalPlace, "Odlagalište mora postojati.");

        var disposalPlace = disposalPlaceRepository.findById(id).orElseThrow(() -> new EntityMissingException(String.format("Odlagalište s ID-em %d ne postoji.", id)));

        if (new_disposalPlace.getWorkingHours() != null)
        {
            disposalPlace.setWorkingHours(new_disposalPlace.getWorkingHours());
        }

        if (new_disposalPlace.getWasteCategories() != null)
        {
            Assert.notEmpty(new_disposalPlace.getWasteCategories(), "Kategorije otpada ne smiju biti prazne.");
            disposalPlace.setWasteCategories(new_disposalPlace.getWasteCategories());
        }

        return disposalPlaceRepository.save(disposalPlace);
    }
}
