package com.SpringBoot.service;

import com.SpringBoot.entity.DisposalPlace;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface DisposalPlaceService
{
    List<DisposalPlace> listAll();

    DisposalPlace updateDisposalPlace(Long id, DisposalPlace waste, UserDetails user);

}
