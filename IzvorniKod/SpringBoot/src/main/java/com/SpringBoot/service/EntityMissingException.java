package com.SpringBoot.service;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(NOT_FOUND)
public class EntityMissingException extends RuntimeException
{
    public EntityMissingException(String message)
    {
        super(message);
    }
}
