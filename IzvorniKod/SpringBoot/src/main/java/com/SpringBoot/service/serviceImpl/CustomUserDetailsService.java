package com.SpringBoot.service.serviceImpl;

import com.SpringBoot.entity.CustomPersonDetails;
import com.SpringBoot.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService
{
    @Autowired
    private PersonService personService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        return new CustomPersonDetails(personService.findByUsername(username));
    }
}
