package com.SpringBoot.service;

import com.SpringBoot.entity.Person;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface PersonService
{
    List<Person> listAll();

    Person fetch(String username, UserDetails user);

    Person createPerson(Person person);

    Person findByUsername(String username);

    Person updatePerson(String username, Person person, UserDetails user);

    Person deletePerson(String username, UserDetails user);
}
