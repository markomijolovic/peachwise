package com.SpringBoot;

import com.SpringBoot.dao.*;
import com.SpringBoot.entity.*;
import com.SpringBoot.entity.enums.Role;
import com.SpringBoot.entity.enums.WasteCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class DataInitializer
{
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private DisposalPlaceRepository disposalPlaceRepository;
    @Autowired
    private RemovalRepository removalRepository;
    @Autowired
    private CompanyRepository companyRepository;

    @Value("${opp.test.person.usernames}")
    private String usernames;

    @Value("${opp.test.person.passwords}")
    private String passwords;

    @Value("${opp.test.person.roles}")
    private String roles;

    @Value("${opp.test.product.names}")
    private String productNames;

    @Value("${opp.test.product.wasteCategories}")
    private String productWasteCategories;

    @Value("${opp.test.disposalplace.addresses}")
    private String dpAddresses;

    @Value("${opp.test.disposalplace.workingHours}")
    private String dpWorkingHours;

    @Value("${opp.test.disposalplace.wasteCategories}")
    private String dpWastes;

    @Value("${opp.test.removal.cityBlocks}")
    private String removalCityBlocks;

    @Value("${opp.test.removal.time}")
    private String removalTimes;

    @Value("${opp.test.removal.wasteCategory}")
    private String removalWasteCategories;

    @Value("${opp.test.companyData}")
    private String companyData;

    @EventListener
    public void appReady(ApplicationReadyEvent event)
    {
        loadPersons();

        loadProducts();

        loadDisposalPlaces();

        loadRemovals();

        loadCompany();
    }

    private void loadCompany()
    {
        if (!companyRepository.findAll().isEmpty())
        {
            return;
        }

        var dataArr = companyData.split(",");

        var company = new Company();

        company.setName(dataArr[0]);
        company.setAddress(dataArr[1]);
        company.setPhoneNumber(dataArr[2]);
        company.setEmail(dataArr[3]);

        companyRepository.save(company);
    }

    private void loadProducts()
    {
        var names = productNames.split(",");
        var wasteCats = productWasteCategories.split(",");

        for (int i = 0; i < names.length; i++)
        {
            if (productRepository.existsByName(names[i]))
            {
                continue;
            }
            var product = new Product();
            product.setName(names[i]);
            product.setWasteCategory(WasteCategory.valueOf(wasteCats[i]));
            productRepository.save(product);
        }
    }

    private void loadDisposalPlaces()
    {
        var addresses = dpAddresses.split(",");
        var workingHours = dpWorkingHours.split(",");
        var wastes = dpWastes.split(",");

        for (int i = 0; i < addresses.length; i++)
        {
            if (disposalPlaceRepository.existsByAddress(addresses[i]))
            {
                continue;
            }
            var dp = new DisposalPlace();
            dp.setWasteCategories(new ArrayList<WasteCategory>());
            dp.setAddress(addresses[i]);
            dp.setWorkingHours(workingHours[i]);

            var individual_wastes = wastes[i].split(" ");

            for (var str : individual_wastes)
            {
                dp.getWasteCategories().add(WasteCategory.valueOf(str));
            }

            disposalPlaceRepository.save(dp);
        }
    }

    private void loadRemovals()
    {
        var cityBlocks = removalCityBlocks.split(",");
        var times = removalTimes.split(",");
        var wasteCategories = removalWasteCategories.split(",");

        for (int i = 0; i < cityBlocks.length; i++)
        {
            var cityBlock = cityBlocks[i];
            var wasteCategory = WasteCategory.valueOf(wasteCategories[i]);

            if (removalRepository.existsByCityBlockAndWasteCategory(cityBlock, wasteCategory))
            {
                continue;
            }
            var removal = new Removal();
            removal.setCityBlock(cityBlock);
            removal.setTime(times[i]);
            removal.setWasteCategory(wasteCategory);

            removalRepository.save(removal);
        }
    }

    private void loadPersons()
    {
        var names = usernames.split(",");
        var pass = passwords.split(",");
        var rol = roles.split(",");

        for (int i = 0; i < names.length; i++)
        {
            if (personRepository.existsByUsername(names[i]))
            {
                continue;
            }
            var person = new Person();
            person.setUsername(names[i]);
            person.setPassword(passwordEncoder.encode(pass[i]));
            person.setRole(Role.valueOf(rol[i]));

            personRepository.save(person);
        }
    }
}
